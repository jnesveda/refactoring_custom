"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomIsAbstractCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomIsAbstractCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomIsAbstractCodeGenerator new
! !

!CustomIsAbstractCodeGeneratorTests methodsFor:'tests'!

test_available_in_context

    self deny: (generatorOrRefactoring class availableInContext:context).

    context selectedClasses: (Array with: self class).

    self assert: (generatorOrRefactoring class availableInContext:context).

    "Modified (format): / 15-11-2014 / 15:51:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_in_perspective_class
    
    self assert: (generatorOrRefactoring class availableInPerspective: CustomPerspective classPerspective)

    "Modified: / 15-11-2014 / 15:52:10 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_is_abstract_method_exists
    | expectedSource metaclass class |

    metaclass := model createClass
        name: #DummyClass01;
        compile;
        theMetaclass.

    metaclass compile: 'isAbstract ^ false'.

    expectedSource := 'isAbstract ^ false'.

    context selectedClasses: (Array with: metaclass).
    generatorOrRefactoring executeInContext: context.  

    class := Smalltalk at: #DummyClass01.
    self assertClassMethodSource:expectedSource atSelector:#isAbstract forClass:class

    "Created: / 16-11-2014 / 17:25:51 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_is_abstract_method_generated
    |expectedSource|

    expectedSource := 'isAbstract
    ^ self == DummyClassForGeneratorTestCase'.

    self executeGeneratorInContext:#classWithInstanceVariable. 

    self assertClassMethodSource:expectedSource atSelector:#isAbstract

    "Created: / 15-11-2014 / 15:53:35 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 15-11-2014 / 19:32:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_is_abstract_method_generated_for_metaclass
    | expectedSource metaclass class |

    metaclass := model createClass
        name: #DummyClass01;
        compile;
        theMetaclass.

    expectedSource := 'isAbstract
    ^ self == DummyClass01'.

    context selectedClasses: (Array with: metaclass).
    generatorOrRefactoring executeInContext: context.  

    class := Smalltalk at: #DummyClass01.
    self assertClassMethodSource:expectedSource atSelector:#isAbstract forClass:class

    "Created: / 16-11-2014 / 17:17:27 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

