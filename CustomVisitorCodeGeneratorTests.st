"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomVisitorCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomVisitorCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomVisitorCodeGenerator new
! !

!CustomVisitorCodeGeneratorTests methodsFor:'tests'!

test_accept_visitor_method_generated_with_comment_and_with_parameter
    "check if methods for visitor pattern are correctly generated"
    | expectedSource class |

    userPreferences generateComments: true.

    expectedSource := 'acceptVisitor:visitor with:parameter 
    "Double dispatch back to the visitor, passing my type encoded in
the selector (visitor pattern)"
    "stub code automatically generated - please change if required"

    ^ visitor visitDummyTestClassForVisitorMethod:self with:parameter'.

    class := model createClassImmediate: 'DummyTestClassForVisitorMethod'.

    generatorOrRefactoring
        buildAcceptVisitorMethod: 'visitDummyTestClassForVisitorMethod:'
        withParameter: true 
        forClass: class.

    generatorOrRefactoring model execute.

    self assertMethodSource: expectedSource atSelector: #acceptVisitor:with: forClass: class

    "Created: / 27-07-2014 / 12:32:22 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 29-08-2014 / 20:59:10 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_accept_visitor_method_generated_with_comment_and_without_parameter
    "check if methods for visitor pattern are correctly generated"
    | expectedSource class |

    userPreferences generateComments: true.

    expectedSource := 'acceptVisitor:visitor 
    "Double dispatch back to the visitor, passing my type encoded in
the selector (visitor pattern)"
    "stub code automatically generated - please change if required"

    ^ visitor visitDummyTestClassForVisitorMethod:self'.

    class := model createClassImmediate: 'DummyTestClassForVisitorMethod'.

    generatorOrRefactoring
        buildAcceptVisitorMethod: 'visitDummyTestClassForVisitorMethod:'
        withParameter: false 
        forClass: class.

    generatorOrRefactoring model execute.

    self assertMethodSource: expectedSource atSelector: #acceptVisitor: forClass: class

    "Created: / 03-08-2014 / 22:50:06 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 29-08-2014 / 20:59:36 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_accept_visitor_method_generated_without_comment_and_with_parameter
    "check if methods for visitor pattern are correctly generated"
    | expectedSource class |

    userPreferences generateComments: false.

    expectedSource := 'acceptVisitor:visitor with:parameter 
    ^ visitor visitDummyTestClassForVisitorMethod:self with:parameter'.

    class := model createClassImmediate: 'DummyTestClassForVisitorMethod'.

    generatorOrRefactoring
        buildAcceptVisitorMethod: 'visitDummyTestClassForVisitorMethod:'
        withParameter: true 
        forClass: class.

    generatorOrRefactoring model execute.

    self assertMethodSource: expectedSource atSelector: #acceptVisitor:with: forClass: class

    "Created: / 03-08-2014 / 22:53:36 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 29-08-2014 / 20:59:55 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_accept_visitor_method_generated_without_comment_and_without_parameter
    "check if methods for visitor pattern are correctly generated"
    | expectedSource class |

    userPreferences generateComments: false.  

    expectedSource := 'acceptVisitor:visitor
    ^ visitor visitDummyTestClassForVisitorMethod:self'.

    class := model createClassImmediate: 'DummyTestClassForVisitorMethod'.

    generatorOrRefactoring
        buildAcceptVisitorMethod: 'visitDummyTestClassForVisitorMethod:'
        withParameter: false 
        forClass: class.

    generatorOrRefactoring model execute.

    self assertMethodSource: expectedSource atSelector: #acceptVisitor: forClass: class

    "Created: / 03-08-2014 / 22:51:18 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 29-08-2014 / 21:00:06 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

