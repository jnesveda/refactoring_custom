"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomNewClassGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomNewClassGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    |generator|

    generator := mock mockOf: CustomNewClassGenerator.
    generator class compileMockMethod: 'description ^ ''some description''. '.
    ^ generator

    "Modified: / 09-11-2014 / 00:36:28 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomNewClassGeneratorTests methodsFor:'tests'!

test_available_in_context
    
    self assert: (generatorOrRefactoring class availableInContext: context)

    "Modified: / 09-11-2014 / 00:17:07 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_in_perspective
    
    self assert: (generatorOrRefactoring class availableInPerspective: CustomPerspective classPerspective)

    "Modified (comment): / 09-11-2014 / 00:18:00 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_is_abstract
    
    self assert: CustomNewClassGenerator isAbstract

    "Modified (comment): / 09-11-2014 / 00:30:43 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_new_class_generated

    generatorOrRefactoring
        compileMockMethod: 'buildForClass: aClass ^ self';
        newClassName: #DummyClass01.

    generatorOrRefactoring executeInContext: context.  

    self assertClassExists: #DummyClass01.

    "Created: / 09-11-2014 / 00:32:14 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_new_class_generated_with_dialog

    dialog := mock mockOf: CustomDialog.
    dialog compileMockMethod: 'requestClassName: aLabel initialAnswer: anAnswer

        self assert: aLabel = ''some label''.
        self assert: anAnswer = ''DummyClass01''.

        ^ anAnswer. '.

    generatorOrRefactoring
        compileMockMethod: 'buildForClass: aClass ^ aClass superclassName: #CustomCodeGenerator';
        compileMockMethod: 'newClassNameLabel ^ ''some label''. ';
        compileMockMethod: 'defaultClassName ^ ''DummyClass01''. ';
        dialog: dialog.

    context := CustomBrowserContext new.

    generatorOrRefactoring executeInContext: context.  

    self assertClassExists: #DummyClass01.

    "Created: / 09-11-2014 / 00:48:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 12-11-2014 / 23:41:08 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_new_class_generated_with_subclass_responsibility
    | expectedSource |

    generatorOrRefactoring
        compileMockMethod: 'buildForClass: aClass ^ aClass superclassName: #CustomCodeGenerator';
        newClassName: #DummyClass01.

    generatorOrRefactoring executeInContext: context.  

    self assertClassExists: #DummyClass01.

    expectedSource := 'buildInContext:aCustomContext
    self shouldImplement'.

    self assertMethodSource: expectedSource atSelector: #buildInContext: forClass: (Smalltalk at: #DummyClass01).

    "Created: / 09-11-2014 / 00:41:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

