"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomTestCaseTearDownCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomTestCaseTearDownCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomTestCaseTearDownCodeGenerator new
! !

!CustomTestCaseTearDownCodeGeneratorTests methodsFor:'tests'!

test_tear_down_method_generated_default_package
    | expectedPackage class actualPackage |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        package: #dummy_package01;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedPackage := #dummy_package01.
    generatorOrRefactoring executeInContext: context.
    actualPackage := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #tearDown) package.  
    self assert: expectedPackage = actualPackage

    "Created: / 31-01-2015 / 21:41:26 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_tear_down_method_generated_not_same_package
    | expectedPackage class actualPackage |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        package: #dummy_package01;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedPackage := PackageId noProjectID.
    generatorOrRefactoring samePackageAsTestedClass: false.  
    generatorOrRefactoring executeInContext: context.
    actualPackage := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #tearDown) package.  
    self assert: expectedPackage = actualPackage

    "Created: / 31-01-2015 / 21:42:32 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_tear_down_method_generated_same_method_protocol  
    | class expectedCategory actualCategory |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedCategory := (TestCase compiledMethodAt: #tearDown) category.
    generatorOrRefactoring executeInContext: context.
    actualCategory := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #tearDown) category.
    self assert: expectedCategory = actualCategory

    "Created: / 31-01-2015 / 21:42:23 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_tear_down_method_generated_same_package
    | expectedPackage class actualPackage |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        package: #dummy_package01;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedPackage := #dummy_package01.
    generatorOrRefactoring samePackageAsTestedClass: true.  
    generatorOrRefactoring executeInContext: context.
    actualPackage := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #tearDown) package.  
    self assert: expectedPackage = actualPackage

    "Created: / 31-01-2015 / 21:42:12 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_tear_down_method_generated_tear_down_implemented
    | expectedSource class |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedSource := 'tearDown
    "Add your own code here..."

    super tearDown.'.
    generatorOrRefactoring executeInContext: context.  
    self assertMethodSource: expectedSource atSelector: #tearDown forClass: class

    "Created: / 31-01-2015 / 21:42:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_tear_down_method_generated_tear_down_not_implemented
    | expectedSource class |

    class := model createClass
        name: #DummyTestCase01;
        compile;
        yourself.

    context selectedClasses: {class}.  

    expectedSource := 'tearDown
    "Add your own code here..."
    "/ super tearDown.'.
    generatorOrRefactoring buildForClass: class.  
    self assertMethodSource: expectedSource atSelector: #tearDown forClass: class

    "Created: / 31-01-2015 / 21:41:48 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

