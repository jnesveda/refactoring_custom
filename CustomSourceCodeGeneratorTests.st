"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

TestCase subclass:#CustomSourceCodeGeneratorTests
	instanceVariableNames:'sourceCodeGenerator'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Tests'
!

!CustomSourceCodeGeneratorTests methodsFor:'initialization & release'!

setUp

    sourceCodeGenerator := CustomSourceCodeGenerator new.
    sourceCodeGenerator formatter: CustomNoneSourceCodeFormatter new.

    "Modified: / 19-09-2014 / 23:42:00 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSourceCodeGeneratorTests methodsFor:'tests'!

test_new_source_literal_replacement
    |expectedSource actualSource|

    actualSource := sourceCodeGenerator
            replace:'`"comment1' with:'"comment1"';
            replace:'`"comment2' with:'"other comment2"';
            replace:'`#literal' with:'''some info''';
            source:'selector
    `"comment1

    self information: `#literal.

    `"comment2

    ^ 55';
            newSource.
    expectedSource := 'selector
    "comment1"

    self information: ''some info''.

    "other comment2"

    ^ 55'.
    self assert:expectedSource = actualSource.

    "Created: / 19-09-2014 / 23:45:30 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_new_source_selector_as_symbol
    | expectedSource actualSource |

    actualSource := sourceCodeGenerator
        source: '`@selector
            self shouldImplement';
        replace: '`@selector' with: 'aSelector: withParam' asSymbol;
        newSource.    

    expectedSource := 'aSelector: withParam
            self shouldImplement'.

    self assert: expectedSource = actualSource.

    "Created: / 20-09-2014 / 09:36:39 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_new_source_selector_replacement
    |expectedSource actualSource|

    actualSource := sourceCodeGenerator
            source:'`@selector
            self shouldImplement';
            replace:'`@selector' with:'aSelector';
            newSource.
    expectedSource := 'aSelector
            self shouldImplement'.
    self assert:expectedSource = actualSource.

    "Created: / 19-09-2014 / 23:51:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_new_source_selector_with_param
    | expectedSource actualSource |

    actualSource := sourceCodeGenerator
        source: '`@selector
            self shouldImplement';
        replace: '`@selector' with: 'aSelector: withParam';
        newSource.    

    expectedSource := 'aSelector: withParam
            self shouldImplement'.

    self assert: expectedSource = actualSource.

    "Created: / 20-09-2014 / 09:39:15 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_replace_comments_in_source
    | expectedSource actualSource |

    actualSource := sourceCodeGenerator
        replace: '`"comment1' with: '"comment1"'; 
        replace: '`"comment2' with: '"other comment2"';
        replaceCommentsInSource:'selector
    `"comment1

    self information: ''some info''.

    `"comment2

    ^ 55'.    

    expectedSource := 'selector
    "comment1"

    self information: ''some info''.

    "other comment2"

    ^ 55'.
    
    self assert: expectedSource = actualSource.

    "Created: / 19-09-2014 / 22:50:29 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

