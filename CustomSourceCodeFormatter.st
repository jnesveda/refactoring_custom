"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

Object subclass:#CustomSourceCodeFormatter
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom'
!

!CustomSourceCodeFormatter class methodsFor:'documentation'!

documentation
"
    Defines required methods for a source code formatter implementation used in this package.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>
"
! !

!CustomSourceCodeFormatter class methodsFor:'instance creation'!

new
    "return an initialized instance"

    ^ self basicNew initialize.
! !

!CustomSourceCodeFormatter methodsFor:'formatting'!

formatParseTree: aParseTree
    "Should return parse tree formatted as source code "

    self subclassResponsibility

    "Modified (comment): / 28-08-2014 / 22:13:38 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

