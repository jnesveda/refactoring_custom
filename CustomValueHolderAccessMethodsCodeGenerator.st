"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomValueHolderAccessMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomValueHolderAccessMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"
    
    ^ 'Access Method(s) for ValueHolder and selected instance variables'

    "Modified: / 13-07-2014 / 17:00:47 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."
    
    ^ 'Access Method(s) for ValueHolder'

    "Modified: / 13-07-2014 / 17:00:29 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomValueHolderAccessMethodsCodeGenerator methodsFor:'executing'!

buildInContext: aCustomContext
    "Creates access methods XX for given context"

    self executeSubGeneratorOrRefactoringClasses:(Array 
                  with:CustomValueHolderGetterMethodsCodeGenerator
                  with:CustomSimpleSetterMethodsCodeGenerator)
          inContext:aCustomContext

    "Modified: / 13-07-2014 / 16:59:23 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

