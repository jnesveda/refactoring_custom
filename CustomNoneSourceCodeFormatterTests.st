"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

TestCase subclass:#CustomNoneSourceCodeFormatterTests
	instanceVariableNames:'formatter'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Tests'
!


!CustomNoneSourceCodeFormatterTests methodsFor:'initialization & release'!

setUp

    formatter := CustomNoneSourceCodeFormatter new

    "Modified: / 31-08-2014 / 15:04:23 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomNoneSourceCodeFormatterTests methodsFor:'tests'!

test_format_parse_tree_source_is_nil
    | expectedSource actualSource parseTree source |

    source := 'selector ^ 777'.
    parseTree := RBParser parseMethod: source.
    parseTree source: nil.

    actualSource := (formatter formatParseTree: parseTree) copyWithRegex: '\s' matchesReplacedWith: ''.    
    expectedSource := 'selector^777'.

    self assert: actualSource = expectedSource.

    "Created: / 31-08-2014 / 15:06:17 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_format_parse_tree_source_not_nil
    | expectedSource actualSource parseTree source |

    source := 'selector ^ 777'.
    parseTree := RBParser parseMethod: source.
    parseTree source: source.

    actualSource := formatter formatParseTree: parseTree.    
    expectedSource := 'selector ^ 777'.

    self assert: actualSource = expectedSource.

    "Created: / 31-08-2014 / 15:07:25 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomNoneSourceCodeFormatterTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

