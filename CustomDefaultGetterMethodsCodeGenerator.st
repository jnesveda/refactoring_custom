"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomDefaultGetterMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!


!CustomDefaultGetterMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description

    ^ 'Getter methods for default variable value in metaclass'

    "Created: / 30-06-2014 / 11:14:23 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

group
    "Returns a collection strings describing a group to which
     receiver belongs. A groups may be nested hence the array of
     strings. For example for subgroup 'Accessors' in group 'Generators'
     this method should return #('Generators' 'Accessors')."

    "/ By default return an empty array which means the item will appear
    "/ in top-level group.
    ^ #('Accessors' 'Getters')

    "Created: / 22-08-2014 / 18:54:37 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label

    ^ 'Getter Method(s) for default variable value'

    "Created: / 30-06-2014 / 11:15:07 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomDefaultGetterMethodsCodeGenerator methodsFor:'code generation'!

sourceForClass: aClass variableName: aName
    "Returns getter method source code for default variable value"

    | comment |

    comment := ''.

    userPreferences generateCommentsForGetters ifTrue:[
        comment := '"default value for the ''%1'' instance variable (automatically generated)"'.
        comment := comment bindWith: aName.
    ].

    ^ self sourceCodeGenerator
        source: '`@methodName
            `"comment

            self shouldImplement.
            ^ nil';
        replace: '`@methodName' with: (self defaultMethodNameFor: aName) asSymbol;
        replace: '`"comment' with: comment;
        newSource.

    "Created: / 30-06-2014 / 10:49:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 19-09-2014 / 22:34:45 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomDefaultGetterMethodsCodeGenerator methodsFor:'protected'!

methodClass: aClass
    "Assure that method is generated only for metaclass."

    ^ aClass theMetaclass

    "Created: / 30-06-2014 / 11:11:00 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomDefaultGetterMethodsCodeGenerator class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

