"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGenerator subclass:#CustomSubclassResponsibilityCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!


!CustomSubclassResponsibilityCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"

    ^ 'Generates method stubs marked by: self subclassResponsibility'

    "Modified: / 08-04-2014 / 21:40:35 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."

    ^ 'Subclass responsibility methods'

    "Modified: / 08-04-2014 / 21:38:02 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSubclassResponsibilityCodeGenerator class methodsFor:'queries'!

availableInContext: aCustomContext
    "Returns true if the generator/refactoring is available in given
     context, false otherwise. 

     Called by the UI to figure out what generators / refactorings 
     are available at given point. See class CustomContext for details."

    ^ aCustomContext selectedClasses notEmptyOrNil

!

availableInPerspective:aCustomPerspective 
    "Returns true if the generator/refactoring is available in given
     perspective, false otherwise.

     Called by the UI to figure out what generators / refactorings
     to show"

    ^ aCustomPerspective isClassPerspective

! !

!CustomSubclassResponsibilityCodeGenerator methodsFor:'code generation'!

createSubclassResponsibilityForClass: aClass

    | missingRequiredSelectors classQuery |

    classQuery := CustomClassQuery new.
    self determineIsClassAbstract: aClass.
    missingRequiredSelectors := CodeGeneratorTool missingRequiredProtocolFor: aClass.

    missingRequiredSelectors isCollection ifTrue: [
        missingRequiredSelectors do: [ :selector |
            | superclassMethod |

            superclassMethod := (classQuery methodForSuperclassSelector: selector class: aClass).

            model createMethod
                class: aClass;
                protocol: superclassMethod category;
                source: ('`@selector

                    self shouldImplement');
                replace: '`@selector' with: superclassMethod methodDefinitionTemplate asSymbol;
                compile. 
        ]
    ]

    "Created: / 09-04-2014 / 20:16:55 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 14-12-2014 / 23:19:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSubclassResponsibilityCodeGenerator methodsFor:'executing'!

buildInContext:aCustomContext

    aCustomContext selectedClasses do: [ :class |
        self
            createSubclassResponsibilityForClass: class theNonMetaclass;
            createSubclassResponsibilityForClass: class theMetaclass.
    ]

    "Created: / 08-04-2014 / 21:33:32 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 13-05-2014 / 22:04:39 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSubclassResponsibilityCodeGenerator methodsFor:'private'!

determineIsClassAbstract: aClass
    "For model class (RBClass, RBMetaclass) determines if the class is abstract or not.
    Original RBAbstractClass >> isAbstract implementation returns mostly wrong guess,
    especially for newly created classes."

    aClass isBehavior ifTrue: [
        "Nothing to do a for real class"
        ^ self
    ].

    aClass isDefined ifTrue: [ 
        aClass isAbstract: aClass realClass isAbstract
    ] ifFalse: [ 
        "For newly created class lets assume thats not abstract"
        aClass isAbstract: false  
    ].

    "Created: / 14-12-2014 / 23:19:29 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSubclassResponsibilityCodeGenerator class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

