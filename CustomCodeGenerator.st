"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoring subclass:#CustomCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!


!CustomCodeGenerator class methodsFor:'testing'!

isAbstract
    ^ self == CustomCodeGenerator

    "Created: / 26-01-2014 / 21:38:59 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

isCustomCodeGenerator
    ^ true
! !

!CustomCodeGenerator methodsFor:'testing'!

isCustomCodeGenerator
    ^ true
! !

!CustomCodeGenerator class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

