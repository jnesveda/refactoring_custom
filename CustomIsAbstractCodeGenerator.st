"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGenerator subclass:#CustomIsAbstractCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomIsAbstractCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"

    ^ 'Mark class as abstract (by implementing class method #isAbstract)'

    "Created: / 20-03-2014 / 01:31:33 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."

    ^ 'Mark class as abstract'

    "Created: / 20-03-2014 / 01:32:28 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomIsAbstractCodeGenerator class methodsFor:'queries'!

availableInContext:aCustomContext
    "Returns true if the generator/refactoring is available in given
     context, false otherwise.
     
     Called by the UI to figure out what generators / refactorings
     are available at given point. See class CustomContext for details."

    ^ aCustomContext selectedClasses notEmptyOrNil

    "Created: / 20-03-2014 / 01:34:11 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

availableInPerspective:aCustomPerspective
    "Returns true if the generator/refactoring is available in given
     perspective, false otherwise.
     
     Called by the UI to figure out what generators / refactorings
     to show"

    ^ aCustomPerspective isClassPerspective

    "Created: / 20-03-2014 / 01:33:48 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomIsAbstractCodeGenerator methodsFor:'executing'!

buildInContext:aCustomContext
    | classes |

    classes := aCustomContext selectedClasses.
    classes do:[:class |
        | metaclass className |

        metaclass := class theMetaclass.
        className := class theNonMetaclass name.

        (metaclass includesSelector: #isAbstract) ifFalse:[  
            self compile: ('isAbstract
    ^ self == %1' bindWith: className) forClass: metaclass inCategory: 'testing'
        ].   
    ]

    "Created: / 16-09-2014 / 07:20:23 / Jan Vrany <jan.vrany@fit.cvut.cz>"
    "Modified: / 16-11-2014 / 17:23:31 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

