"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomTestCaseSetUpCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomTestCaseSetUpCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomTestCaseSetUpCodeGenerator new
! !

!CustomTestCaseSetUpCodeGeneratorTests methodsFor:'tests'!

test_set_up_method_generated_default_package
    | expectedPackage class actualPackage |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        package: #dummy_package01;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedPackage := #dummy_package01.
    generatorOrRefactoring executeInContext: context.
    actualPackage := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #setUp) package.  
    self assert: expectedPackage = actualPackage

    "Created: / 31-01-2015 / 21:26:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_set_up_method_generated_not_same_package
    | expectedPackage class actualPackage |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        package: #dummy_package01;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedPackage := PackageId noProjectID.
    generatorOrRefactoring samePackageAsTestedClass: false.  
    generatorOrRefactoring executeInContext: context.
    actualPackage := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #setUp) package.  
    self assert: expectedPackage = actualPackage

    "Created: / 31-01-2015 / 21:31:21 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_set_up_method_generated_same_method_protocol  
    | class expectedCategory actualCategory |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedCategory := (TestCase compiledMethodAt: #setUp) category.
    generatorOrRefactoring executeInContext: context.
    actualCategory := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #setUp) category.
    self assert: expectedCategory = actualCategory

    "Created: / 31-01-2015 / 21:36:48 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_set_up_method_generated_same_package
    | expectedPackage class actualPackage |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        package: #dummy_package01;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedPackage := #dummy_package01.
    generatorOrRefactoring samePackageAsTestedClass: true.  
    generatorOrRefactoring executeInContext: context.
    actualPackage := ((Smalltalk at: #DummyTestCase01) compiledMethodAt: #setUp) package.  
    self assert: expectedPackage = actualPackage

    "Created: / 31-01-2015 / 21:30:22 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_set_up_method_generated_set_up_implemented
    | expectedSource class |

    class := model createClass
        name: #DummyTestCase01;
        superclassName: #TestCase;
        compile;
        yourself. 

    context selectedClasses: {class}.  

    expectedSource := 'setUp
    super setUp.

    "Add your own code here..."'.
    generatorOrRefactoring executeInContext: context.  
    self assertMethodSource: expectedSource atSelector: #setUp forClass: class

    "Created: / 31-01-2015 / 20:30:22 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_set_up_method_generated_set_up_not_implemented
    | expectedSource class |

    class := model createClass
        name: #DummyTestCase01;
        compile;
        yourself.

    context selectedClasses: {class}.  

    expectedSource := 'setUp
    "/ super setUp.
    "Add your own code here..."'.
    generatorOrRefactoring buildForClass: class.  
    self assertMethodSource: expectedSource atSelector: #setUp forClass: class

    "Created: / 31-01-2015 / 20:30:55 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

