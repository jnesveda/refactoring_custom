"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomChangeManager subclass:#CustomLocalChangeManager
	instanceVariableNames:'executedChanges'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom'
!

!CustomLocalChangeManager class methodsFor:'documentation'!

documentation
"
    Unlike CustomBrowserChangeManager this implementation should not affect
    global Browser changes collector, Browser menus and the change file (st.chg).

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz> 

"
! !

!CustomLocalChangeManager class methodsFor:'instance creation'!

new

    ^ self basicNew initialize

    "Created: / 31-05-2014 / 19:34:06 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomLocalChangeManager methodsFor:'initialization'!

initialize

    executedChanges := OrderedCollection new.

    "Created: / 31-05-2014 / 19:31:45 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomLocalChangeManager methodsFor:'performing-changes'!

performChange: aRefactoringChange
    "Applies code change (add/change/remove class/method ...) to the system"

    Class withoutUpdatingChangesDo:[ 
        executedChanges add: aRefactoringChange execute
    ]

    "Modified: / 30-11-2014 / 18:21:15 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

undoChanges
    "undo all changes made by performChange: method"

    Class withoutUpdatingChangesDo:[ 
        executedChanges reversed do: [ :change | 
            change execute
        ]
    ].

    executedChanges removeAll

    "Created: / 19-10-2014 / 14:59:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 30-11-2014 / 15:40:38 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

