"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomVisitorCodeGeneratorAcceptVisitorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomVisitorCodeGeneratorAcceptVisitorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomVisitorCodeGeneratorAcceptVisitor new
! !

!CustomVisitorCodeGeneratorAcceptVisitorTests methodsFor:'tests'!

test_accept_visitor_method_generated_with_comments
    |expectedSource|

    userPreferences generateComments: true.  

    expectedSource := 'acceptVisitor:visitor
    "Double dispatch back to the visitor, passing my type encoded in
the selector (visitor pattern)"
    "stub code automatically generated - please change if required"

    ^ visitor visitDummyClassForGeneratorTestCase:self'.

    self executeGeneratorInContext:#classWithInstanceVariable.
    self assertMethodSource:expectedSource atSelector:#acceptVisitor:

    "Created: / 03-08-2014 / 23:06:24 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 29-08-2014 / 21:00:21 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_accept_visitor_method_generated_without_comments
    |expectedSource|

    userPreferences generateComments: false.  

    expectedSource := 'acceptVisitor:visitor
    ^ visitor visitDummyClassForGeneratorTestCase:self'.

    self executeGeneratorInContext:#classWithInstanceVariable.
    self assertMethodSource:expectedSource atSelector:#acceptVisitor:

    "Created: / 03-08-2014 / 23:33:55 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 29-08-2014 / 21:13:41 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

