"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomValueHolderWithChangeNotificationAccessMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomValueHolderWithChangeNotificationAccessMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator new
! !

!CustomValueHolderWithChangeNotificationAccessMethodsCodeGeneratorTests methodsFor:'tests'!

test_value_holder_access_methods_generated_with_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: true;
        generateCommentsForSetters: true.

    expectedGetterSource := 'instanceVariable
    "return/create the ''instanceVariable'' value holder with change notification (automatically generated)"

    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
        instanceVariable addDependent:self.
    ].
    ^ instanceVariable'. 

    expectedSetterSource := 'instanceVariable:something
    "set the ''instanceVariable'' value holder (automatically generated)"

    |oldValue newValue|

    instanceVariable notNil ifTrue:[
        oldValue := instanceVariable value.
        instanceVariable removeDependent:self.
    ].
    instanceVariable := something.
    instanceVariable notNil ifTrue:[
        instanceVariable addDependent:self.
    ].
    newValue := instanceVariable value.
    oldValue ~~ newValue ifTrue:[
        self
            update:#value
            with:newValue
            from:instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 13-07-2014 / 17:16:44 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_value_holder_access_methods_generated_without_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: false;
        generateCommentsForSetters: false.

    expectedGetterSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
        instanceVariable addDependent:self.
    ].
    ^ instanceVariable'. 

    expectedSetterSource := 'instanceVariable:something
    |oldValue newValue|

    instanceVariable notNil ifTrue:[
        oldValue := instanceVariable value.
        instanceVariable removeDependent:self.
    ].
    instanceVariable := something.
    instanceVariable notNil ifTrue:[
        instanceVariable addDependent:self.
    ].
    newValue := instanceVariable value.
    oldValue ~~ newValue ifTrue:[
        self
            update:#value
            with:newValue
            from:instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 13-07-2014 / 17:23:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomValueHolderWithChangeNotificationAccessMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

