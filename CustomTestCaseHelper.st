"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

Object subclass:#CustomTestCaseHelper
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Helpers'
!

!CustomTestCaseHelper class methodsFor:'documentation'!

documentation
"
    Helper class with utility methods used in TestCase generators/refactoring.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz> 
"
! !

!CustomTestCaseHelper methodsFor:'helpers'!

testCategory: aCategory
    "Returns category based on given category in which should be TestCase located
    e.g. from Category creates Category-Tests"
    | suffix |

    suffix := '-Tests'.

    (aCategory asString endsWith: suffix) ifTrue: [
        ^ aCategory    
    ].

    ^ aCategory asString, suffix.

    "Created: / 30-08-2014 / 20:01:21 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified (comment): / 08-11-2014 / 21:09:11 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

