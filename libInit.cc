/*
 * $Header$
 *
 * DO NOT EDIT
 * automagically generated from the projectDefinition: jn_refactoring_custom.
 */
#define __INDIRECTVMINITCALLS__
#include <stc.h>

#ifdef WIN32
# pragma codeseg INITCODE "INITCODE"
#endif

#if defined(INIT_TEXT_SECTION) || defined(DLL_EXPORT)
DLL_EXPORT void _libjn_refactoring_custom_Init() INIT_TEXT_SECTION;
DLL_EXPORT void _libjn_refactoring_custom_InitDefinition() INIT_TEXT_SECTION;
#endif

void _libjn_refactoring_custom_InitDefinition(pass, __pRT__, snd)
OBJ snd; struct __vmData__ *__pRT__; {
__BEGIN_PACKAGE2__("libjn_refactoring_custom__DFN", _libjn_refactoring_custom_InitDefinition, "jn:refactoring_custom");
_jn_137refactoring_137custom_Init(pass,__pRT__,snd);

__END_PACKAGE__();
}

void _libjn_refactoring_custom_Init(pass, __pRT__, snd)
OBJ snd; struct __vmData__ *__pRT__; {
__BEGIN_PACKAGE2__("libjn_refactoring_custom", _libjn_refactoring_custom_Init, "jn:refactoring_custom");
_CustomChangeManager_Init(pass,__pRT__,snd);
_CustomClassQuery_Init(pass,__pRT__,snd);
_CustomCodeGeneratorOrRefactoring_Init(pass,__pRT__,snd);
_CustomContext_Init(pass,__pRT__,snd);
_CustomDialog_Init(pass,__pRT__,snd);
_CustomManager_Init(pass,__pRT__,snd);
_CustomMenuBuilder_Init(pass,__pRT__,snd);
_CustomMock_Init(pass,__pRT__,snd);
_CustomNamespace_Init(pass,__pRT__,snd);
_CustomParseTreeRewriter_Init(pass,__pRT__,snd);
_CustomPerspective_Init(pass,__pRT__,snd);
_CustomRefactoryBuilder_Init(pass,__pRT__,snd);
_CustomSourceCodeFormatter_Init(pass,__pRT__,snd);
_CustomSourceCodeGenerator_Init(pass,__pRT__,snd);
_CustomSourceCodeSelection_Init(pass,__pRT__,snd);
_CustomTestCaseHelper_Init(pass,__pRT__,snd);
_jn_137refactoring_137custom_Init(pass,__pRT__,snd);
_CustomBrowserChangeManager_Init(pass,__pRT__,snd);
_CustomBrowserContext_Init(pass,__pRT__,snd);
_CustomCodeGenerator_Init(pass,__pRT__,snd);
_CustomLocalChangeManager_Init(pass,__pRT__,snd);
_CustomNoneSourceCodeFormatter_Init(pass,__pRT__,snd);
_CustomRBLocalSourceCodeFormatter_Init(pass,__pRT__,snd);
_CustomRefactoring_Init(pass,__pRT__,snd);
_CustomSilentDialog_Init(pass,__pRT__,snd);
_CustomSubContext_Init(pass,__pRT__,snd);
_CustomUserDialog_Init(pass,__pRT__,snd);
_CustomAccessMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomCodeSelectionRefactoring_Init(pass,__pRT__,snd);
_CustomIsAbstractCodeGenerator_Init(pass,__pRT__,snd);
_CustomJavaSimpleSetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomNewClassGenerator_Init(pass,__pRT__,snd);
_CustomReplaceIfNilWithIfTrueRefactoring_Init(pass,__pRT__,snd);
_CustomSubclassResponsibilityCodeGenerator_Init(pass,__pRT__,snd);
_CustomTestCaseCodeGenerator_Init(pass,__pRT__,snd);
_CustomTestCaseMethodCodeGenerator_Init(pass,__pRT__,snd);
_CustomTestCaseSetUpCodeGenerator_Init(pass,__pRT__,snd);
_CustomTestCaseTearDownCodeGenerator_Init(pass,__pRT__,snd);
_CustomUpdateTestCaseCategoryRefactoring_Init(pass,__pRT__,snd);
_CustomVisitorCodeGenerator_Init(pass,__pRT__,snd);
_CustomChangeNotificationAccessMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomChangeNotificationSetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomCodeGeneratorClassGenerator_Init(pass,__pRT__,snd);
_CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator_Init(pass,__pRT__,snd);
_CustomCodeSelectionToResourceTranslation_Init(pass,__pRT__,snd);
_CustomDefaultGetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomLazyInitializationAccessMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomLazyInitializationGetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomMultiSetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomPrintCodeSelectionRefactoring_Init(pass,__pRT__,snd);
_CustomRefactoringClassGenerator_Init(pass,__pRT__,snd);
_CustomSimpleAccessMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomSimpleGetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomSimpleSetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomUITestCaseCodeGenerator_Init(pass,__pRT__,snd);
_CustomUITestCaseSetUpCodeGenerator_Init(pass,__pRT__,snd);
_CustomValueHolderAccessMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomValueHolderGetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator_Init(pass,__pRT__,snd);
_CustomVisitorCodeGeneratorAcceptVisitor_Init(pass,__pRT__,snd);
_CustomJavaScriptSimpleSetterMethodsCodeGenerator_Init(pass,__pRT__,snd);

_jn_137refactoring_137custom_extensions_Init(pass,__pRT__,snd);
__END_PACKAGE__();
}
