"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoring subclass:#CustomRefactoring
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Refactorings'
!


!CustomRefactoring class methodsFor:'testing'!

isAbstract
    ^ self == CustomRefactoring

    "Created: / 26-01-2014 / 21:39:14 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

isCustomRefactoring
    ^ true
! !

!CustomRefactoring methodsFor:'testing'!

isCustomRefactoring
    ^ true
! !

!CustomRefactoring class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

