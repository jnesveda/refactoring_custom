"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomNewClassGenerator subclass:#CustomCodeGeneratorClassGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!


!CustomCodeGeneratorClassGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"

    ^ 'Generates template class for code generator'

    "Modified: / 29-03-2014 / 19:38:48 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

group
    "Returns a collection strings describing a group to which
     receiver belongs. A groups may be nested hence the array of
     strings. For example for subgroup 'Accessors' in group 'Generators'
     this method should return #('Generators' 'Accessors')."

    "/ By default return an empty array which means the item will appear
    "/ in top-level group.
    ^ #(Generators)

    "Created: / 22-08-2014 / 18:48:04 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."

    ^ 'New Code Generator'

    "Modified: / 29-03-2014 / 19:21:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 22-08-2014 / 18:50:45 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomCodeGeneratorClassGenerator methodsFor:'accessing - ui'!

defaultClassName

    ^ 'CustomXXXCodeGenerator'

    "Created: / 09-11-2014 / 01:13:37 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

newClassNameLabel

    ^ 'Enter class name for new generator'

    "Created: / 09-11-2014 / 01:14:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeGeneratorClassGenerator methodsFor:'executing - private'!

buildForClass: aClass

    aClass
        superclassName: #CustomCodeGenerator;
        category: self class category.

    "Created: / 09-11-2014 / 01:15:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeGeneratorClassGenerator class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

