"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGenerator subclass:#CustomTestCaseSetUpCodeGenerator
	instanceVariableNames:'samePackageAsTestedClass'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomTestCaseSetUpCodeGenerator class methodsFor:'accessing-presentation'!

description
    ^ 'Generates initial #setUp for test cases'

    "Modified: / 16-09-2014 / 11:24:20 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

group
    "Returns a collection strings describing a group to which
     receiver belongs. A groups may be nested hence the array of
     strings. For example for subgroup 'Accessors' in group 'Generators'
     this method should return #('Generators' 'Accessors')."

    "/ By default return an empty array which means the item will appear
    "/ in top-level group.
    ^ #('Testing')

    "Created: / 05-08-2014 / 14:14:20 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label
    ^ 'TestCase setUp method'

    "Modified: / 05-08-2014 / 14:13:33 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomTestCaseSetUpCodeGenerator class methodsFor:'queries'!

availableInContext:aCustomContext
    ^ aCustomContext selectedClasses anySatisfy: [:cls | cls isMetaclass not and:[ cls inheritsFrom: TestCase ] ]

    "Modified: / 05-08-2014 / 14:24:16 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

availableInPerspective:aCustomPerspective
    ^ aCustomPerspective isClassPerspective

    "Modified: / 05-08-2014 / 13:49:03 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomTestCaseSetUpCodeGenerator methodsFor:'accessing'!

samePackageAsTestedClass
    "Returns true when we should assign TestCase class 
    to the same package as tested class."

    ^ samePackageAsTestedClass

    "Created: / 15-11-2014 / 11:54:37 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

samePackageAsTestedClass: aBoolean
    "see samePackageAsTestedClass"

    samePackageAsTestedClass := aBoolean

    "Created: / 15-11-2014 / 11:56:36 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomTestCaseSetUpCodeGenerator methodsFor:'accessing - defaults'!

defaultSamePackageAsTestedClass
    "default value for samePackageAsTestedClass"

    ^ true

    "Created: / 15-11-2014 / 12:21:40 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomTestCaseSetUpCodeGenerator methodsFor:'executing'!

buildForClass: class
    | source category superHasSetup current package |

    current := class.
    superHasSetup := false.
    [ superHasSetup not and:[ current isNil not ] ] whileTrue:[
        superHasSetup := current includesSelector: #setUp.
        superHasSetup ifFalse:[
            current := current superclass.
        ].
    ].
    superHasSetup ifTrue:[ 
        source := 'setUp
    super setUp.

    "Add your own code here..."
'.
        category := (current compiledMethodAt: #setUp) category.
    ] ifFalse:[ 
        source := 'setUp
    "/ super setUp.

    "Add your own code here..."
'.
        category := (TestCase compiledMethodAt: #setUp) category.
    ].

    package := PackageId noProjectID.
    samePackageAsTestedClass ? self defaultSamePackageAsTestedClass ifTrue: [ 
        package := class package
    ].

    model createMethod
        class: class;
        source: source;
        category: category;
        package: package;
        compile.

    "Created: / 05-08-2014 / 14:17:17 / Jan Vrany <jan.vrany@fit.cvut.cz>"
    "Modified: / 31-01-2015 / 18:32:53 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

buildInContext:aCustomContext
    aCustomContext selectedClasses do:[:cls |  
        (cls isMetaclass not and:[ cls inheritsFrom: TestCase ]) ifTrue:[ 
            self buildForClass: cls.
        ].
    ]

    "Modified: / 05-08-2014 / 14:24:34 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

