"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomSourceCodeFormatter subclass:#CustomNoneSourceCodeFormatter
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom'
!

!CustomNoneSourceCodeFormatter class methodsFor:'documentation'!

documentation
"
    Should keep original source code formatting even after code replacements - see CustomSourceCodeGenerator.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>
"
! !

!CustomNoneSourceCodeFormatter methodsFor:'formatting'!

formatParseTree:aParseTree
    "Returns parse tree with (possibly) original/unchanged formatting"

    aParseTree source isNil ifTrue:[
        "no source - try to build from parse tree "
        ^ aParseTree formattedCode
    ] ifFalse: [
        ^ aParseTree source
    ]

    "Modified: / 29-08-2014 / 22:26:22 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

