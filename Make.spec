# $Header$
#
# DO NOT EDIT
# automagically generated from the projectDefinition: jn_refactoring_custom.
#
# Warning: once you modify this file, do not rerun
# stmkmp or projectDefinition-build again - otherwise, your changes are lost.
#
# This file contains specifications which are common to all platforms.
#

# Do NOT CHANGE THESE DEFINITIONS
# (otherwise, ST/X will have a hard time to find out the packages location from its packageID,
#  to find the source code of a class and to find the library for a package)
MODULE=jn
MODULE_DIR=refactoring_custom
PACKAGE=$(MODULE):$(MODULE_DIR)


# Argument(s) to the stc compiler (stc --usage).
#  -headerDir=. : create header files locally
#                (if removed, they will be created as common
#  -Pxxx       : defines the package
#  -Zxxx       : a prefix for variables within the classLib
#  -Dxxx       : defines passed to to CC for inline C-code
#  -Ixxx       : include path passed to CC for inline C-code
#  +optspace   : optimized for space
#  +optspace2  : optimized more for space
#  +optspace3  : optimized even more for space
#  +optinline  : generate inline code for some ST constructs
#  +inlineNew  : additionally inline new
#  +inlineMath : additionally inline some floatPnt math stuff
#
# ********** OPTIONAL: MODIFY the next line(s) ***
# STCLOCALOPTIMIZATIONS=+optinline +inlineNew
# STCLOCALOPTIMIZATIONS=+optspace3
STCLOCALOPTIMIZATIONS=+optspace3


# Argument(s) to the stc compiler (stc --usage).
#  -warn            : no warnings
#  -warnNonStandard : no warnings about ST/X extensions
#  -warnEOLComments : no warnings about EOL comment extension
#  -warnPrivacy     : no warnings about privateClass extension
#  -warnUnused      : no warnings about unused variables
#
# ********** OPTIONAL: MODIFY the next line(s) ***
# STCWARNINGS=-warn
# STCWARNINGS=-warnNonStandard
# STCWARNINGS=-warnEOLComments
STCWARNINGS=-warnNonStandard

COMMON_CLASSES= \
	CustomChangeManager \
	CustomClassQuery \
	CustomCodeGeneratorOrRefactoring \
	CustomContext \
	CustomDialog \
	CustomManager \
	CustomMenuBuilder \
	CustomMock \
	CustomNamespace \
	CustomParseTreeRewriter \
	CustomPerspective \
	CustomRefactoryBuilder \
	CustomSourceCodeFormatter \
	CustomSourceCodeGenerator \
	CustomSourceCodeSelection \
	CustomTestCaseHelper \
	jn_refactoring_custom \
	CustomBrowserChangeManager \
	CustomBrowserContext \
	CustomCodeGenerator \
	CustomLocalChangeManager \
	CustomNoneSourceCodeFormatter \
	CustomRBLocalSourceCodeFormatter \
	CustomRefactoring \
	CustomSilentDialog \
	CustomSubContext \
	CustomUserDialog \
	CustomAccessMethodsCodeGenerator \
	CustomCodeSelectionRefactoring \
	CustomIsAbstractCodeGenerator \
	CustomJavaSimpleSetterMethodsCodeGenerator \
	CustomNewClassGenerator \
	CustomReplaceIfNilWithIfTrueRefactoring \
	CustomSubclassResponsibilityCodeGenerator \
	CustomTestCaseCodeGenerator \
	CustomTestCaseMethodCodeGenerator \
	CustomTestCaseSetUpCodeGenerator \
	CustomTestCaseTearDownCodeGenerator \
	CustomUpdateTestCaseCategoryRefactoring \
	CustomVisitorCodeGenerator \
	CustomChangeNotificationAccessMethodsCodeGenerator \
	CustomChangeNotificationSetterMethodsCodeGenerator \
	CustomCodeGeneratorClassGenerator \
	CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator \
	CustomCodeSelectionToResourceTranslation \
	CustomDefaultGetterMethodsCodeGenerator \
	CustomLazyInitializationAccessMethodsCodeGenerator \
	CustomLazyInitializationGetterMethodsCodeGenerator \
	CustomMultiSetterMethodsCodeGenerator \
	CustomPrintCodeSelectionRefactoring \
	CustomRefactoringClassGenerator \
	CustomSimpleAccessMethodsCodeGenerator \
	CustomSimpleGetterMethodsCodeGenerator \
	CustomSimpleSetterMethodsCodeGenerator \
	CustomUITestCaseCodeGenerator \
	CustomUITestCaseSetUpCodeGenerator \
	CustomValueHolderAccessMethodsCodeGenerator \
	CustomValueHolderGetterMethodsCodeGenerator \
	CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator \
	CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator \
	CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator \
	CustomVisitorCodeGeneratorAcceptVisitor \
	CustomJavaScriptSimpleSetterMethodsCodeGenerator \




COMMON_OBJS= \
    $(OUTDIR_SLASH)CustomChangeManager.$(O) \
    $(OUTDIR_SLASH)CustomClassQuery.$(O) \
    $(OUTDIR_SLASH)CustomCodeGeneratorOrRefactoring.$(O) \
    $(OUTDIR_SLASH)CustomContext.$(O) \
    $(OUTDIR_SLASH)CustomDialog.$(O) \
    $(OUTDIR_SLASH)CustomManager.$(O) \
    $(OUTDIR_SLASH)CustomMenuBuilder.$(O) \
    $(OUTDIR_SLASH)CustomMock.$(O) \
    $(OUTDIR_SLASH)CustomNamespace.$(O) \
    $(OUTDIR_SLASH)CustomParseTreeRewriter.$(O) \
    $(OUTDIR_SLASH)CustomPerspective.$(O) \
    $(OUTDIR_SLASH)CustomRefactoryBuilder.$(O) \
    $(OUTDIR_SLASH)CustomSourceCodeFormatter.$(O) \
    $(OUTDIR_SLASH)CustomSourceCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomSourceCodeSelection.$(O) \
    $(OUTDIR_SLASH)CustomTestCaseHelper.$(O) \
    $(OUTDIR_SLASH)jn_refactoring_custom.$(O) \
    $(OUTDIR_SLASH)CustomBrowserChangeManager.$(O) \
    $(OUTDIR_SLASH)CustomBrowserContext.$(O) \
    $(OUTDIR_SLASH)CustomCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomLocalChangeManager.$(O) \
    $(OUTDIR_SLASH)CustomNoneSourceCodeFormatter.$(O) \
    $(OUTDIR_SLASH)CustomRBLocalSourceCodeFormatter.$(O) \
    $(OUTDIR_SLASH)CustomRefactoring.$(O) \
    $(OUTDIR_SLASH)CustomSilentDialog.$(O) \
    $(OUTDIR_SLASH)CustomSubContext.$(O) \
    $(OUTDIR_SLASH)CustomUserDialog.$(O) \
    $(OUTDIR_SLASH)CustomAccessMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomCodeSelectionRefactoring.$(O) \
    $(OUTDIR_SLASH)CustomIsAbstractCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomJavaSimpleSetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomNewClassGenerator.$(O) \
    $(OUTDIR_SLASH)CustomReplaceIfNilWithIfTrueRefactoring.$(O) \
    $(OUTDIR_SLASH)CustomSubclassResponsibilityCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomTestCaseCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomTestCaseMethodCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomTestCaseSetUpCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomTestCaseTearDownCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomUpdateTestCaseCategoryRefactoring.$(O) \
    $(OUTDIR_SLASH)CustomVisitorCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomChangeNotificationAccessMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomChangeNotificationSetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomCodeGeneratorClassGenerator.$(O) \
    $(OUTDIR_SLASH)CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomCodeSelectionToResourceTranslation.$(O) \
    $(OUTDIR_SLASH)CustomDefaultGetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomLazyInitializationAccessMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomLazyInitializationGetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomMultiSetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomPrintCodeSelectionRefactoring.$(O) \
    $(OUTDIR_SLASH)CustomRefactoringClassGenerator.$(O) \
    $(OUTDIR_SLASH)CustomSimpleAccessMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomSimpleGetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomSimpleSetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomUITestCaseCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomUITestCaseSetUpCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomValueHolderAccessMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomValueHolderGetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)CustomVisitorCodeGeneratorAcceptVisitor.$(O) \
    $(OUTDIR_SLASH)CustomJavaScriptSimpleSetterMethodsCodeGenerator.$(O) \
    $(OUTDIR_SLASH)extensions.$(O) \



