"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomJavaScriptSimpleSetterMethodsCodeGeneratorTests
	instanceVariableNames:'javaScriptClass'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomJavaScriptSimpleSetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomJavaScriptSimpleSetterMethodsCodeGenerator new
! !

!CustomJavaScriptSimpleSetterMethodsCodeGeneratorTests methodsFor:'initialization & release'!

setUp
    | classDefinition |

    super setUp.

    classDefinition := 'public class DummyJavaStriptClass01 extends Object {
    var instVar01;
}'.

    Class withoutUpdatingChangesDo: [   
        JavaScriptCompiler evaluate: classDefinition notifying: nil compile: nil
    ].

    javaScriptClass := Smalltalk at: #DummyJavaStriptClass01.

    "Modified: / 01-02-2015 / 16:22:44 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

tearDown

    Class withoutUpdatingChangesDo: [   
        javaScriptClass removeFromSystem
    ].
    "super tearDown."
    "Call of undoing changes produces error relevant
    to parsing JavaScript source code as Smalltalk code."
    "changeManager undoChanges."
    mock unmockAll

    "Modified: / 01-02-2015 / 16:21:27 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomJavaScriptSimpleSetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_simple_setter_generated_with_comment
    | expectedSource |

    userPreferences generateComments: true.

    context selectedClasses: {javaScriptClass}.  

    expectedSource := 'instVar01(something) {
    // set the value of the instance variable ''instVar01'' (automatically generated)
    instVar01 = something;
}'.

    generatorOrRefactoring executeInContext: context.

    self assertMethodSource: expectedSource atSelector: #'instVar01:' forClass: javaScriptClass

    "Created: / 01-02-2015 / 16:24:34 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_simple_setter_generated_without_comment
    | expectedSource |

    userPreferences generateComments: false.

    context selectedClasses: {javaScriptClass}.  

    expectedSource := 'instVar01(something) {
    instVar01 = something;
}'.

    generatorOrRefactoring executeInContext: context.

    self assertMethodSource: expectedSource atSelector: #'instVar01:' forClass: javaScriptClass

    "Created: / 01-02-2015 / 16:26:05 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

