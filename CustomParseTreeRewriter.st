"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

ParseTreeSourceRewriter subclass:#CustomParseTreeRewriter
	instanceVariableNames:'oldSource newSource'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom'
!

!CustomParseTreeRewriter class methodsFor:'documentation'!

documentation
"
    Extension for ParseTreeSourceRewriter so that it work even for expressions and not just methods.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>

"
! !

!CustomParseTreeRewriter methodsFor:'accessing'!

executeTree: aParseTree
    "Like ParseTreeSourceRewriter >> executeTree:, but with
    additional support for expressions"
    | oldContext treeFromRewrittenSource |

    oldContext := context.
    context := (RBSmallDictionary ? Dictionary) new.
    answer := false.
    oldSource isNil ifTrue: [
        oldSource := aParseTree source
    ].  

    oldSource isNil ifTrue: [ 
        self error: 'We need the oldSource string to be set'.
    ].

    "/Rewrite the tree as usual and then (try to) rewrite the original source code
    tree := self visitNode: aParseTree.
    replacements notNil ifTrue:[
        newSource := self executeReplacementsInSource: oldSource.
    ] ifFalse:[
        ^answer
    ].
    "/DO NOT forget rewrites here!!!!!!"

    "/Now, validates that rewritten parse tree is the same as
    "/the one we get from the rewritten source:
    aParseTree isMethod ifTrue: [ 
        treeFromRewrittenSource := RBParser parseRewriteMethod: newSource onError:[:error :position|nil].
    ] ifFalse: [ 
        treeFromRewrittenSource := RBParser parseExpression: newSource onError:[:error :position|nil].
    ].
    treeFromRewrittenSource = tree ifTrue:[
        (tree respondsTo: #source:) ifTrue: [ 
            tree source: newSource.
        ].
    ] ifFalse: [
        "Better set newSource to nil in order to indicate that something went wrong"
        newSource := nil.
    ].
    context := oldContext.
    ^answer

    "Created: / 09-12-2014 / 21:16:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 05-02-2015 / 22:07:20 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

newSource
    "Returns the source code string after the replacements were executed"

    ^ newSource

    "Modified (comment): / 09-12-2014 / 22:19:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

oldSource
    "Returns custom old source code string - the code before replacements were made"

    ^ oldSource

    "Modified (comment): / 09-12-2014 / 21:19:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

oldSource: aString
    "see oldSource"

    oldSource := aString

    "Modified (comment): / 09-12-2014 / 21:19:59 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

