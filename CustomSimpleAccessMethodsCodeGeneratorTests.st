"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomSimpleAccessMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomSimpleAccessMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomSimpleAccessMethodsCodeGenerator new
! !

!CustomSimpleAccessMethodsCodeGeneratorTests methodsFor:'tests'!

test_access_methods_generated_with_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: true;
        generateCommentsForSetters: true.

    expectedGetterSource := 'instanceVariable
    "return the instance variable ''instanceVariable'' (automatically generated)"

    ^ instanceVariable'.

    expectedSetterSource := 'instanceVariable:something
    "set the value of the instance variable ''instanceVariable'' (automatically generated)"

    instanceVariable := something'.


    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 08-07-2014 / 13:28:26 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 08-07-2014 / 16:02:01 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_access_methods_generated_without_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: false;
        generateCommentsForSetters: false.

    expectedGetterSource := 'instanceVariable
    ^ instanceVariable'.

    expectedSetterSource := 'instanceVariable:something
    instanceVariable := something'.


    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 08-07-2014 / 16:59:10 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

