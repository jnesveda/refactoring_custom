"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

Object subclass:#CustomChangeManager
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom'
!

!CustomChangeManager class methodsFor:'documentation'!

documentation
"
    Interface for operations with refactory browser changes needed for CustomCodeGenerator.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>

"
! !

!CustomChangeManager class methodsFor:'testing'!

isAbstract
    ^ self == CustomChangeManager
! !

!CustomChangeManager methodsFor:'performing-changes'!

performChange: aRefactoringChange

    self subclassResponsibility

    "Created: / 31-05-2014 / 13:02:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomChangeManager class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

