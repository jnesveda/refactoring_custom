"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomValueHolderWithChangeNotificationGetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomValueHolderWithChangeNotificationGetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator new
! !

!CustomValueHolderWithChangeNotificationGetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_value_holder_getter_with_change_notification_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: true.

    expectedSource := 'instanceVariable
    "return/create the ''instanceVariable'' value holder with change notification (automatically generated)"

    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
        instanceVariable addDependent:self.
    ].
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 30-06-2014 / 19:54:41 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_value_holder_getter_with_change_notification_method_generated_without_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: false.

    expectedSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
        instanceVariable addDependent:self.
    ].
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 30-06-2014 / 19:56:11 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomValueHolderWithChangeNotificationGetterMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

