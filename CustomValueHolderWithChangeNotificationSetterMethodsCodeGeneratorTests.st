"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomValueHolderWithChangeNotificationSetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomValueHolderWithChangeNotificationSetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator new
! !

!CustomValueHolderWithChangeNotificationSetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_value_holder_with_change_notification_setter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForSetters: true.

    expectedSource := 'instanceVariable:something
    "set the ''instanceVariable'' value holder (automatically generated)"

    |oldValue newValue|

    instanceVariable notNil ifTrue:[
        oldValue := instanceVariable value.
        instanceVariable removeDependent:self.
    ].
    instanceVariable := something.
    instanceVariable notNil ifTrue:[
        instanceVariable addDependent:self.
    ].
    newValue := instanceVariable value.
    oldValue ~~ newValue ifTrue:[
        self
            update:#value
            with:newValue
            from:instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable:

    "Created: / 07-07-2014 / 00:24:37 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_value_holder_with_change_notification_setter_method_generated_without_comments
    | expectedSource |

    userPreferences generateCommentsForSetters: false.

    expectedSource := 'instanceVariable:something
    |oldValue newValue|

    instanceVariable notNil ifTrue:[
        oldValue := instanceVariable value.
        instanceVariable removeDependent:self.
    ].
    instanceVariable := something.
    instanceVariable notNil ifTrue:[
        instanceVariable addDependent:self.
    ].
    newValue := instanceVariable value.
    oldValue ~~ newValue ifTrue:[
        self
            update:#value
            with:newValue
            from:instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable:

    "Created: / 07-07-2014 / 00:23:41 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomValueHolderWithChangeNotificationSetterMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

