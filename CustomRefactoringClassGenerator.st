"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomNewClassGenerator subclass:#CustomRefactoringClassGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomRefactoringClassGenerator class methodsFor:'accessing-presentation'!

description

    ^ 'Create new class which should perform some refactoring'

    "Modified: / 09-11-2014 / 00:51:47 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

group

    ^ #(Generators)

    "Created: / 08-11-2014 / 17:19:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label

    ^ 'New Refactoring'

    "Modified: / 08-11-2014 / 17:18:28 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomRefactoringClassGenerator methodsFor:'accessing - ui'!

defaultClassName

    ^ 'CustomXXXRefactoring'

    "Created: / 08-11-2014 / 17:15:49 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

newClassNameLabel

    ^ 'Enter class name for new refactoring'

    "Created: / 08-11-2014 / 17:16:37 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomRefactoringClassGenerator methodsFor:'executing - private'!

buildForClass: aClass

    aClass
        superclassName: #CustomRefactoring;
        category: CustomRefactoring category

    "Created: / 08-11-2014 / 17:15:10 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

