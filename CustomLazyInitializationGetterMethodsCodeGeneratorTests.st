"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomLazyInitializationGetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomLazyInitializationGetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomLazyInitializationGetterMethodsCodeGenerator new
! !

!CustomLazyInitializationGetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_lazy_initialization_getter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: true.

    expectedSource := 'instanceVariable
    "return the instance instance variable ''instanceVariable'' with lazy instance creation (automatically generated)"

    instanceVariable isNil ifTrue:[
        instanceVariable := self class defaultInstanceVariable.
    ].
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 30-06-2014 / 18:03:02 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_lazy_initialization_getter_method_generated_with_default_method
    | expectedGetterSource expectedDefaultSource |

    userPreferences generateCommentsForGetters: false.

    expectedGetterSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := self class defaultInstanceVariable.
    ].
    ^ instanceVariable'.

    expectedDefaultSource := 'defaultInstanceVariable
    self shouldImplement.
    ^ nil'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertClassMethodSource: expectedDefaultSource atSelector: #defaultInstanceVariable

    "Created: / 30-06-2014 / 18:06:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 11-07-2014 / 20:11:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_lazy_initialization_getter_method_generated_without_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: false.

    expectedSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := self class defaultInstanceVariable.
    ].
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 30-06-2014 / 10:37:21 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

