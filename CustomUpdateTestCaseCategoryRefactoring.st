"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomRefactoring subclass:#CustomUpdateTestCaseCategoryRefactoring
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Refactorings'
!

!CustomUpdateTestCaseCategoryRefactoring class methodsFor:'accessing-presentation'!

description

    ^ 'Updates TestCase subclasses category with respect to their classes to be tested'

    "Modified: / 08-11-2014 / 18:49:25 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label

    ^ 'Update TestCase Category'

    "Modified: / 08-11-2014 / 17:28:41 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomUpdateTestCaseCategoryRefactoring class methodsFor:'queries'!

availableForClass:aClass

    ^ (aClass isSubclassOf: TestCase) and: [ 
        aClass theNonMetaclass name endsWith: 'Tests'  
    ]

    "Created: / 08-11-2014 / 19:03:20 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

availableInContext:aCustomContext 

    ^ aCustomContext selectedClasses ? #() anySatisfy: [ :class |
        self availableForClass: class
    ]

    "Modified: / 25-01-2015 / 15:10:12 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

availableInPerspective: aCustomPerspective 

    ^ aCustomPerspective isClassPerspective

    "Modified: / 08-11-2014 / 17:27:06 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomUpdateTestCaseCategoryRefactoring methodsFor:'executing - private'!

buildInContext: aCustomContext 
    | testCaseHelper |

    testCaseHelper := CustomTestCaseHelper new.

    aCustomContext selectedClasses ? #() do:[ :class | 
        (self class availableForClass: class) ifTrue: [ 
            | testedClassName |

            testedClassName := (class theNonMetaclass name removeSuffix: 'Tests') asSymbol.
            (model includesClassNamed: testedClassName) ifTrue: [
                | testCategory |

                testCategory := testCaseHelper 
                    testCategory: (model classNamed: testedClassName) category.

                (testCategory = (class category)) ifFalse: [ 
                    refactoryBuilder changeCategoryOf: class to: testCategory
                ]
            ]
        ]
    ]

    "Modified: / 08-11-2014 / 21:33:18 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

