"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomSimpleSetterMethodsCodeGenerator subclass:#CustomJavaScriptSimpleSetterMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomJavaScriptSimpleSetterMethodsCodeGenerator class methodsFor:'documentation'!

documentation
"
    Same purpose like superclass, but for STX JavaScript language.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>
"
! !

!CustomJavaScriptSimpleSetterMethodsCodeGenerator class methodsFor:'queries'!

availableForProgrammingLanguages

    ^ {STXJavaScriptLanguage instance}

    "Created: / 31-01-2015 / 18:16:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomJavaScriptSimpleSetterMethodsCodeGenerator methodsFor:'code generation'!

sourceForClass: aClass variableName: aName
    "Returns simple setter for given STX JavaScript class and variable name."

    | methodName comment argName |

    methodName := aName.
    argName := self argNameForMethodName: methodName.  
    comment := ''.

    userPreferences generateCommentsForSetters ifTrue:[
        | varType |

        varType := self varTypeOf: aName class: aClass. 
        comment := '
    // set the value of the %1 variable ''%2'' (automatically generated)'.
        comment := comment bindWith: varType with: aName.
    ].

    ^ methodName, '(', argName, ') {', comment, '
    ', aName, ' = ', argName, ';
}'.

    "Created: / 31-01-2015 / 18:15:11 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

