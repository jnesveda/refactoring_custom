"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomCodeGeneratorClassGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomCodeGeneratorClassGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring

    ^ CustomCodeGeneratorClassGenerator new

    "Created: / 27-09-2014 / 11:27:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 12-11-2014 / 23:17:51 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeGeneratorClassGeneratorTests methodsFor:'tests'!

test_code_generator_class_created

    | newGenerator testClassName |

    testClassName := #CustomCodeGeneratorForTestCase.

    self assertClassNotExists: testClassName.

    generatorOrRefactoring dialog answer: testClassName forSelector: #requestClassName:initialAnswer:.

    generatorOrRefactoring executeInContext: CustomBrowserContext new.

    newGenerator := Smalltalk classNamed: testClassName.

    self assertClassExists: testClassName.
    self assert: (newGenerator includesSelector: #buildInContext:).
    self assert: (newGenerator class includesSelector: #buildInContext:) not.
    self assert: (newGenerator includesSelector: #availableInContext:) not.
    self assert: (newGenerator class includesSelector: #availableInContext:).

    "Created: / 31-03-2014 / 23:20:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 12-11-2014 / 23:22:59 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeGeneratorClassGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

