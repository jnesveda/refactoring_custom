"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeSelectionRefactoring subclass:#CustomPrintCodeSelectionRefactoring
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Refactorings'
!

!CustomPrintCodeSelectionRefactoring class methodsFor:'accessing-presentation'!

description

    ^ 'Wraps selected source code with Transcript showCR: to be printed out'

    "Modified: / 15-11-2014 / 16:21:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label

    ^ 'Wrap with Transcript showCR: '

    "Modified: / 15-11-2014 / 16:21:05 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomPrintCodeSelectionRefactoring methodsFor:'executing - private'!

buildInContext:aCustomContext

    refactoryBuilder 
          replace:'`@expression'
          with:'(Transcript showCR: (`@expression) asString)'
          inContext:aCustomContext

    "Modified: / 15-11-2014 / 16:36:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

