"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeSelectionRefactoring subclass:#CustomCodeSelectionToResourceTranslation
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Refactorings'
!

!CustomCodeSelectionToResourceTranslation class methodsFor:'documentation'!

documentation
"
    Wraps variable, literal, string or expression code selection
    with following translation call: resources string:'Some string...'
    Ie.: when 'Some string...' is selected in the code editor then its
    replaced by: resources string:'Some string...'

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>

"
! !

!CustomCodeSelectionToResourceTranslation class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"

    ^ 'Wrap code selection with translation call - resources string:'

    "Created: / 21-08-2014 / 23:46:18 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."

    ^ 'Selection to Resources Translation'

    "Created: / 21-08-2014 / 23:43:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeSelectionToResourceTranslation methodsFor:'executing'!

buildInContext:aCustomContext
    "Performs a refactoring within given context scope"

    refactoryBuilder 
          replace:'`@expression'
          with:'(resources string: (`@expression))'
          inContext:aCustomContext

    "Created: / 23-08-2014 / 00:16:58 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 05-11-2014 / 22:57:44 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeSelectionToResourceTranslation class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

