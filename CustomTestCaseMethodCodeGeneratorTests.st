"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomTestCaseMethodCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomTestCaseMethodCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomTestCaseMethodCodeGenerator new
! !

!CustomTestCaseMethodCodeGeneratorTests methodsFor:'tests'!

test_generate_test_method_for_class
    | expectedSource method class testClass |

    class := model createClassImmediate: #MockClassForTestCase01 superClassName: #Object.
    testClass := model createClassImmediate: #MockClassForTestCase01Tests.
    method := model createMethodImmediate: class source: 'selector_01 ^ 1'.

    context selectedMethods: (Array with: method).  

    expectedSource := 'test_selector_01
    "/ something selector_01.    

    self assert:1 = 2.'.

    generatorOrRefactoring executeInContext: context.  

    self assertMethodSource:expectedSource atSelector:#test_selector_01 forClass:testClass

    "Created: / 29-11-2014 / 15:04:16 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_generate_test_method_for_private_class
    | expectedSource privateClass method testClass |

    model createClassImmediate: #MockClassForTestCase01 superClassName: #Object.
    privateClass := model createClassImmediate: #PrivateClass01 superClassName: #MockClassForTestCase01 privateIn: #MockClassForTestCase01.
    testClass := model createClassImmediate: #MockClassForTestCase01Tests.
    method := model createMethodImmediate: privateClass source: 'selector_01 ^ 1'.

    context selectedMethods: (Array with: method).  

    expectedSource := 'test_selector_01
    "/ something selector_01.    

    self assert:1 = 2.'.

    generatorOrRefactoring executeInContext: context.  

    self assertMethodSource:expectedSource atSelector:#test_selector_01 forClass:testClass

    "Created: / 31-10-2014 / 00:30:29 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 25-01-2015 / 15:54:08 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

