"{ Package: 'jn:refactoring_custom/patches' }"

"{ NameSpace: Smalltalk }"

Object subclass:#CustomDummyClassPatches
	instanceVariableNames:'className isMeta'
	classVariableNames:''
	poolDictionaries:''
	category:'Refactory-Change Objects'
!

!CustomDummyClassPatches class methodsFor:'documentation'!

documentation
"
    Kludge class - added because all my patches subdirectory wont be included in project definition.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>

"
! !

!CustomDummyClassPatches class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

