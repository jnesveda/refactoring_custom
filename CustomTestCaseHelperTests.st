"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

TestCase subclass:#CustomTestCaseHelperTests
	instanceVariableNames:'testCaseHelper'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Helpers-Tests'
!

!CustomTestCaseHelperTests methodsFor:'initialization & release'!

setUp
    super setUp.

    testCaseHelper := CustomTestCaseHelper new

    "Modified: / 08-11-2014 / 21:11:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomTestCaseHelperTests methodsFor:'tests'!

test_test_category_ending_with_tests
    | expectedCategory actualCategory |

    expectedCategory := 'Some-Category-Tests'.
    actualCategory := testCaseHelper testCategory:'Some-Category-Tests'.    

    self assert: expectedCategory equals: actualCategory.

    "Created: / 30-08-2014 / 22:06:14 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 08-11-2014 / 21:13:10 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_test_category_ending_without_tests
    | expectedCategory actualCategory |

    expectedCategory := 'Some-Category-Tests'.
    actualCategory := testCaseHelper testCategory:'Some-Category'.    

    self assert: expectedCategory equals: actualCategory.

    "Created: / 30-08-2014 / 22:07:13 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 08-11-2014 / 21:13:18 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

