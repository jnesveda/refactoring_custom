"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomSimpleGetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomSimpleGetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomSimpleGetterMethodsCodeGenerator new

    "Modified: / 29-05-2014 / 00:07:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSimpleGetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_simple_getter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: true.

    expectedSource := 'instanceVariable
    "return the instance variable ''instanceVariable'' (automatically generated)"

    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 31-05-2014 / 20:06:05 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 14-06-2014 / 10:41:05 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_simple_getter_method_generated_without_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: false.

    expectedSource := 'instanceVariable
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 31-05-2014 / 20:05:55 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 14-06-2014 / 10:41:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSimpleGetterMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

