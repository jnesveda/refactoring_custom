"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomLazyInitializationAccessMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomLazyInitializationAccessMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomLazyInitializationAccessMethodsCodeGenerator new
! !

!CustomLazyInitializationAccessMethodsCodeGeneratorTests methodsFor:'tests'!

test_lazy_initialization_access_methods_generated_with_comments
    | expectedGetterSource expectedSetterSource expectedDefaultSource |

    userPreferences
        generateCommentsForGetters: true;
        generateCommentsForSetters: true.

    expectedGetterSource := 'instanceVariable
    "return the instance instance variable ''instanceVariable'' with lazy instance creation (automatically generated)"

    instanceVariable isNil ifTrue:[
        instanceVariable := self class defaultInstanceVariable.
    ].
    ^ instanceVariable'.

    expectedSetterSource := 'instanceVariable:something 
    "set the value of the instance variable ''instanceVariable'' and send a change notification (automatically generated)"

    (instanceVariable ~~ something) ifTrue:[
        instanceVariable := something.
        self changed:#instanceVariable.
    ].'.

    expectedDefaultSource := 'defaultInstanceVariable
    "default value for the ''instanceVariable'' instance variable (automatically generated)"

    self shouldImplement.
    ^ nil'.


    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:.
    self assertClassMethodSource: expectedDefaultSource atSelector: #defaultInstanceVariable

    "Created: / 08-07-2014 / 18:43:29 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 11-07-2014 / 20:11:40 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_lazy_initialization_access_methods_generated_without_comments
    | expectedGetterSource expectedSetterSource expectedDefaultSource |

    userPreferences
        generateCommentsForGetters: false;
        generateCommentsForSetters: false.

    expectedGetterSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := self class defaultInstanceVariable.
    ].
    ^ instanceVariable'.

    expectedSetterSource := 'instanceVariable:something 
    (instanceVariable ~~ something) ifTrue:[
        instanceVariable := something.
        self changed:#instanceVariable.
    ].'.

    expectedDefaultSource := 'defaultInstanceVariable
    self shouldImplement.
    ^ nil'.


    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:.
    self assertClassMethodSource: expectedDefaultSource atSelector: #defaultInstanceVariable

    "Created: / 08-07-2014 / 19:37:40 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 11-07-2014 / 20:11:48 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

