"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomValueHolderAccessMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomValueHolderAccessMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomValueHolderAccessMethodsCodeGenerator new
! !

!CustomValueHolderAccessMethodsCodeGeneratorTests methodsFor:'tests'!

test_value_holder_access_methods_generated_with_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: true;
        generateCommentsForSetters: true.

    expectedGetterSource := 'instanceVariable
    "return/create the ''instanceVariable'' value holder (automatically generated)"

    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
    ].
    ^ instanceVariable'. 

    expectedSetterSource := 'instanceVariable:something
    "set the value of the instance variable ''instanceVariable'' (automatically generated)"

    instanceVariable := something'.


    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 13-07-2014 / 17:02:59 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_value_holder_access_methods_generated_without_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: false;
        generateCommentsForSetters: false.

    expectedGetterSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
    ].
    ^ instanceVariable'. 

    expectedSetterSource := 'instanceVariable:something
    instanceVariable := something'.


    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 13-07-2014 / 17:03:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

