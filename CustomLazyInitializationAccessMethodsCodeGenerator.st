"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomLazyInitializationAccessMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomLazyInitializationAccessMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"
    
    ^ 'Access Method(s) with Lazy Initialization in Getter for selected instance variables'

    "Modified: / 08-07-2014 / 18:10:40 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."
    
    ^ 'Access Method(s) with Lazy Initialization in Getter'

    "Modified: / 08-07-2014 / 18:10:06 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomLazyInitializationAccessMethodsCodeGenerator methodsFor:'executing'!

buildInContext: aCustomContext
    "Creates access methods with lazy initialization in getter for given context"

    self executeSubGeneratorOrRefactoringClasses:(Array 
                  with:CustomLazyInitializationGetterMethodsCodeGenerator
                  with:CustomChangeNotificationSetterMethodsCodeGenerator)
          inContext:aCustomContext

    "Modified: / 08-07-2014 / 18:37:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

