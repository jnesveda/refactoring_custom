"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomTestCaseCodeGenerator subclass:#CustomUITestCaseCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!


!CustomUITestCaseCodeGenerator class methodsFor:'accessing-presentation'!

description
    ^ 'Creates a new test case for UI'

    "Created: / 16-09-2014 / 11:33:33 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."

    ^ 'New UI Test Case'

    "Created: / 16-09-2014 / 11:23:32 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomUITestCaseCodeGenerator methodsFor:'accessing - defaults'!

defaultGenerateSetUp
    ^ true

    "Created: / 16-09-2014 / 11:26:00 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

defaultSetUpCodeGeneratorClass
    ^ CustomUITestCaseSetUpCodeGenerator

    "Created: / 16-09-2014 / 11:20:31 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!CustomUITestCaseCodeGenerator class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

