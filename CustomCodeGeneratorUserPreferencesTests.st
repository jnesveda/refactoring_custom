"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomCodeGeneratorUserPreferencesTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Tests'
!

!CustomCodeGeneratorUserPreferencesTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ nil

    "Modified: / 09-06-2014 / 22:42:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomCodeGeneratorUserPreferencesTests methodsFor:'tests'!

test_user_preferences_same_for_tests

    self assert: userPreferences generateComments.
    self assert: userPreferences generateCommentsForAspectMethods.
    self assert: userPreferences generateCommentsForGetters.
    self assert: userPreferences generateCommentsForSetters.

    "Created: / 09-06-2014 / 22:29:31 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 25-01-2015 / 15:49:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

