"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomChangeNotificationSetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomChangeNotificationSetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomChangeNotificationSetterMethodsCodeGenerator new
! !

!CustomChangeNotificationSetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_change_notification_setter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForSetters: true.

    expectedSource := 'instanceVariable:something 
    "set the value of the instance variable ''instanceVariable'' and send a change notification (automatically generated)"

    (instanceVariable ~~ something) ifTrue:[
        instanceVariable := something.
        self changed:#instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable:

    "Created: / 06-07-2014 / 13:55:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_notification_setter_method_generated_without_comments
    | expectedSource |

    userPreferences generateCommentsForSetters: false.

    expectedSource := 'instanceVariable:something 
    (instanceVariable ~~ something) ifTrue:[
        instanceVariable := something.
        self changed:#instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable:

    "Created: / 06-07-2014 / 14:05:58 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_instance_variable_present
    | expectedVariables actualVariables |

    self executeGeneratorInContext: #classWithInstanceVariable.

    expectedVariables := #('instanceVariable').
    actualVariables := (Smalltalk at: #DummyClassForGeneratorTestCase) instanceVariableNames.

    self assert: expectedVariables = actualVariables

    "Created: / 30-11-2014 / 19:16:34 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

