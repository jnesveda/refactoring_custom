"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"
    
    ^ 'Access Method(s) for ValueHolder with Change Notification for selected instance variables'

    "Modified: / 13-07-2014 / 17:13:46 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."
    
    ^ 'Access Method(s) for ValueHolder with Change Notification'

    "Modified: / 13-07-2014 / 17:13:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator methodsFor:'executing'!

buildInContext: aCustomContext
    "Creates access methods XX for given context"

    self executeSubGeneratorOrRefactoringClasses:(Array 
                  with:CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator
                  with:CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator)
          inContext:aCustomContext

    "Modified: / 13-07-2014 / 17:12:58 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

