"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomSimpleAccessMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!


!CustomSimpleAccessMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"
    
    ^ 'Access Method(s) for selected instance variables'

    "Modified: / 07-07-2014 / 22:20:34 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."
    
    ^ 'Access Method(s)'
! !

!CustomSimpleAccessMethodsCodeGenerator methodsFor:'executing'!

buildInContext: aCustomContext
    "Creates access methods for given context"

    self executeSubGeneratorOrRefactoringClasses:(Array 
                  with:CustomSimpleGetterMethodsCodeGenerator
                  with:CustomSimpleSetterMethodsCodeGenerator)
          inContext:aCustomContext

    "Modified: / 11-07-2014 / 21:16:47 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSimpleAccessMethodsCodeGenerator class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

