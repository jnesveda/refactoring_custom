"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

Object subclass:#CustomClassQuery
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Helpers'
!

!CustomClassQuery class methodsFor:'documentation'!

documentation
"
    Helper class for retrieving additional informations from classes.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>

"
! !

!CustomClassQuery methodsFor:'queries'!

methodForSuperclassSelector: aSelector class: aClass
    "retrieve method under given selector in class 
    superclass or in superclass superclass until method is found
    or nil is reached"

    | superclass |

    superclass := aClass superclass.
    [ superclass notNil ] whileTrue: [ 
        | method |

        method := superclass compiledMethodAt: aSelector asSymbol.
        method notNil ifTrue: [ 
            ^ method
        ].
        superclass := superclass superclass.
    ].

    ^ nil

    "Created: / 15-06-2014 / 14:58:31 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 07-10-2014 / 19:50:42 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

