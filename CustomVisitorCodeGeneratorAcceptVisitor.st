"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomVisitorCodeGenerator subclass:#CustomVisitorCodeGeneratorAcceptVisitor
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomVisitorCodeGeneratorAcceptVisitor class methodsFor:'accessing'!

description
    ^ 'Method for visitor pattern - acceptVisitor'.

    "Created: / 09-03-2014 / 20:38:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 27-07-2014 / 00:58:46 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    ^ 'Visitor Method'.

    "Created: / 09-03-2014 / 20:38:57 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomVisitorCodeGeneratorAcceptVisitor methodsFor:'executing'!

buildInContext: aCustomContext
    "Builds acceptVisitor method"

    aCustomContext selectedClasses do: [ :class | 
        | selector |

        selector := ('visit', class nameWithoutPrefix) asMutator.

        self
            buildAcceptVisitorMethod: selector
            withParameter: false
            forClass: class
    ].

    "Created: / 19-03-2014 / 18:32:50 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 03-08-2014 / 23:31:25 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

