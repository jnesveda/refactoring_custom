"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomValueHolderGetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomValueHolderGetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomValueHolderGetterMethodsCodeGenerator new
! !

!CustomValueHolderGetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_value_holder_getter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: true.

    expectedSource := 'instanceVariable
    "return/create the ''instanceVariable'' value holder (automatically generated)"

    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
    ].
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 14-06-2014 / 11:34:45 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 24-06-2014 / 22:44:20 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_value_holder_getter_method_generated_without_comments
    | expectedSource |    

    userPreferences generateCommentsForGetters: false.

    expectedSource := 'instanceVariable
    instanceVariable isNil ifTrue:[
        instanceVariable := ValueHolder new.
    ].
    ^ instanceVariable'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable

    "Created: / 14-06-2014 / 11:36:51 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 24-06-2014 / 22:12:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomValueHolderGetterMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

