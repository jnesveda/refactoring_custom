# $Header$
#
# DO NOT EDIT
# automagically generated from the projectDefinition: jn_refactoring_custom.
#
# Warning: once you modify this file, do not rerun
# stmkmp or projectDefinition-build again - otherwise, your changes are lost.
#
# Notice, that the name bc.mak is historical (from times, when only borland c was supported).
# This file contains make rules for the win32 platform using either borland-bcc or visual-c.
# It shares common definitions with the unix-make in Make.spec.
# The bc.mak supports the following targets:
#    bmake         - compile all st-files to a classLib (dll)
#    bmake clean   - clean all temp files
#    bmake clobber - clean all
#
# Historic Note:
#  this used to contain only rules to make with borland
#    (called via bmake, by "make.exe -f bc.mak")
#  this has changed; it is now also possible to build using microsoft visual c
#    (called via vcmake, by "make.exe -f bc.mak -DUSEVC")
#
TOP=..\..\stx
INCLUDE_TOP=$(TOP)\..



!INCLUDE $(TOP)\rules\stdHeader_bc

!INCLUDE Make.spec

LIBNAME=libjn_refactoring_custom
MODULE_PATH=refactoring_custom
RESFILES=refactoring_custom.$(RES)



LOCALINCLUDES= -I$(INCLUDE_TOP)\jn\refactoring_custom\patches -I$(INCLUDE_TOP)\stx\goodies\refactoryBrowser\changes -I$(INCLUDE_TOP)\stx\goodies\refactoryBrowser\helpers -I$(INCLUDE_TOP)\stx\goodies\refactoryBrowser\parser -I$(INCLUDE_TOP)\stx\goodies\sunit -I$(INCLUDE_TOP)\stx\libbasic -I$(INCLUDE_TOP)\stx\libbasic3 -I$(INCLUDE_TOP)\stx\libcomp -I$(INCLUDE_TOP)\stx\libjava -I$(INCLUDE_TOP)\stx\libjava\tools -I$(INCLUDE_TOP)\stx\libjavascript -I$(INCLUDE_TOP)\stx\libtool -I$(INCLUDE_TOP)\stx\libview -I$(INCLUDE_TOP)\stx\libview2 -I$(INCLUDE_TOP)\stx\libwidg -I$(INCLUDE_TOP)\stx\libwidg2
LOCALDEFINES=

STCLOCALOPT=-package=$(PACKAGE) -I. $(LOCALINCLUDES) -headerDir=. $(STCLOCALOPTIMIZATIONS) $(STCWARNINGS) $(LOCALDEFINES)  -varPrefix=$(LIBNAME)
LOCALLIBS=

OBJS= $(COMMON_OBJS) $(WIN32_OBJS)

ALL::  classLibRule

classLibRule: $(OUTDIR) $(OUTDIR)$(LIBNAME).dll

!INCLUDE $(TOP)\rules\stdRules_bc

# build all mandatory prerequisite packages (containing superclasses) for this package
prereq:
	pushd ..\..\stx\libbasic & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\goodies\refactoryBrowser\changes & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\goodies\refactoryBrowser\helpers & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\goodies\refactoryBrowser\parser & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libbasic2 & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libbasic3 & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libcomp & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libui & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libview & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libview2 & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\goodies\sunit & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libwidg & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libwidg2 & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "
	pushd ..\..\stx\libtool & $(MAKE_BAT) "CFLAGS_LOCAL=$(GLOBALDEFINES) "







test: $(TOP)\goodies\builder\reports\NUL
	pushd $(TOP)\goodies\builder\reports & $(MAKE_BAT)
	$(TOP)\goodies\builder\reports\report-runner.bat -D . -r Builder::TestReport -p $(PACKAGE)
        
clean::
	del *.$(CSUFFIX)


# BEGINMAKEDEPEND --- do not remove this line; make depend needs it
$(OUTDIR)CustomChangeManager.$(O) CustomChangeManager.$(H): CustomChangeManager.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomClassQuery.$(O) CustomClassQuery.$(H): CustomClassQuery.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomCodeGeneratorOrRefactoring.$(O) CustomCodeGeneratorOrRefactoring.$(H): CustomCodeGeneratorOrRefactoring.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomContext.$(O) CustomContext.$(H): CustomContext.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomDialog.$(O) CustomDialog.$(H): CustomDialog.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomManager.$(O) CustomManager.$(H): CustomManager.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomMenuBuilder.$(O) CustomMenuBuilder.$(H): CustomMenuBuilder.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomMock.$(O) CustomMock.$(H): CustomMock.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomNamespace.$(O) CustomNamespace.$(H): CustomNamespace.st $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\helpers\RBNamespace.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomParseTreeRewriter.$(O) CustomParseTreeRewriter.$(H): CustomParseTreeRewriter.st $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\parser\ParseTreeRewriter.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\parser\ParseTreeSearcher.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\parser\ParseTreeSourceRewriter.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\parser\RBProgramNodeVisitor.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomPerspective.$(O) CustomPerspective.$(H): CustomPerspective.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomRefactoryBuilder.$(O) CustomRefactoryBuilder.$(H): CustomRefactoryBuilder.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSourceCodeFormatter.$(O) CustomSourceCodeFormatter.$(H): CustomSourceCodeFormatter.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSourceCodeGenerator.$(O) CustomSourceCodeGenerator.$(H): CustomSourceCodeGenerator.st $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\parser\RBProgramNodeVisitor.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(INCLUDE_TOP)\stx\libtool\CodeGenerator.$(H) $(STCHDR)
$(OUTDIR)CustomSourceCodeSelection.$(O) CustomSourceCodeSelection.$(H): CustomSourceCodeSelection.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomTestCaseHelper.$(O) CustomTestCaseHelper.$(H): CustomTestCaseHelper.st $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)jn_refactoring_custom.$(O) jn_refactoring_custom.$(H): jn_refactoring_custom.st $(INCLUDE_TOP)\stx\libbasic\LibraryDefinition.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(INCLUDE_TOP)\stx\libbasic\ProjectDefinition.$(H) $(STCHDR)
$(OUTDIR)CustomBrowserChangeManager.$(O) CustomBrowserChangeManager.$(H): CustomBrowserChangeManager.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomChangeManager.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomBrowserContext.$(O) CustomBrowserContext.$(H): CustomBrowserContext.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomContext.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomCodeGenerator.$(O) CustomCodeGenerator.$(H): CustomCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomLocalChangeManager.$(O) CustomLocalChangeManager.$(H): CustomLocalChangeManager.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomChangeManager.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomNoneSourceCodeFormatter.$(O) CustomNoneSourceCodeFormatter.$(H): CustomNoneSourceCodeFormatter.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomSourceCodeFormatter.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomRBLocalSourceCodeFormatter.$(O) CustomRBLocalSourceCodeFormatter.$(H): CustomRBLocalSourceCodeFormatter.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomSourceCodeFormatter.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomRefactoring.$(O) CustomRefactoring.$(H): CustomRefactoring.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSilentDialog.$(O) CustomSilentDialog.$(H): CustomSilentDialog.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomDialog.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSubContext.$(O) CustomSubContext.$(H): CustomSubContext.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomContext.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomUserDialog.$(O) CustomUserDialog.$(H): CustomUserDialog.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomDialog.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomAccessMethodsCodeGenerator.$(O) CustomAccessMethodsCodeGenerator.$(H): CustomAccessMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomCodeSelectionRefactoring.$(O) CustomCodeSelectionRefactoring.$(H): CustomCodeSelectionRefactoring.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomIsAbstractCodeGenerator.$(O) CustomIsAbstractCodeGenerator.$(H): CustomIsAbstractCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomJavaSimpleSetterMethodsCodeGenerator.$(O) CustomJavaSimpleSetterMethodsCodeGenerator.$(H): CustomJavaSimpleSetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomNewClassGenerator.$(O) CustomNewClassGenerator.$(H): CustomNewClassGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomReplaceIfNilWithIfTrueRefactoring.$(O) CustomReplaceIfNilWithIfTrueRefactoring.$(H): CustomReplaceIfNilWithIfTrueRefactoring.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSubclassResponsibilityCodeGenerator.$(O) CustomSubclassResponsibilityCodeGenerator.$(H): CustomSubclassResponsibilityCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomTestCaseCodeGenerator.$(O) CustomTestCaseCodeGenerator.$(H): CustomTestCaseCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomTestCaseMethodCodeGenerator.$(O) CustomTestCaseMethodCodeGenerator.$(H): CustomTestCaseMethodCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomTestCaseSetUpCodeGenerator.$(O) CustomTestCaseSetUpCodeGenerator.$(H): CustomTestCaseSetUpCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomTestCaseTearDownCodeGenerator.$(O) CustomTestCaseTearDownCodeGenerator.$(H): CustomTestCaseTearDownCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomUpdateTestCaseCategoryRefactoring.$(O) CustomUpdateTestCaseCategoryRefactoring.$(H): CustomUpdateTestCaseCategoryRefactoring.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomVisitorCodeGenerator.$(O) CustomVisitorCodeGenerator.$(H): CustomVisitorCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomChangeNotificationAccessMethodsCodeGenerator.$(O) CustomChangeNotificationAccessMethodsCodeGenerator.$(H): CustomChangeNotificationAccessMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomChangeNotificationSetterMethodsCodeGenerator.$(O) CustomChangeNotificationSetterMethodsCodeGenerator.$(H): CustomChangeNotificationSetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomCodeGeneratorClassGenerator.$(O) CustomCodeGeneratorClassGenerator.$(H): CustomCodeGeneratorClassGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomNewClassGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator.$(O) CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator.$(H): CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomTestCaseCodeGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomCodeSelectionToResourceTranslation.$(O) CustomCodeSelectionToResourceTranslation.$(H): CustomCodeSelectionToResourceTranslation.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeSelectionRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomDefaultGetterMethodsCodeGenerator.$(O) CustomDefaultGetterMethodsCodeGenerator.$(H): CustomDefaultGetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomLazyInitializationAccessMethodsCodeGenerator.$(O) CustomLazyInitializationAccessMethodsCodeGenerator.$(H): CustomLazyInitializationAccessMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomLazyInitializationGetterMethodsCodeGenerator.$(O) CustomLazyInitializationGetterMethodsCodeGenerator.$(H): CustomLazyInitializationGetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomMultiSetterMethodsCodeGenerator.$(O) CustomMultiSetterMethodsCodeGenerator.$(H): CustomMultiSetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomPrintCodeSelectionRefactoring.$(O) CustomPrintCodeSelectionRefactoring.$(H): CustomPrintCodeSelectionRefactoring.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeSelectionRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomRefactoringClassGenerator.$(O) CustomRefactoringClassGenerator.$(H): CustomRefactoringClassGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomNewClassGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSimpleAccessMethodsCodeGenerator.$(O) CustomSimpleAccessMethodsCodeGenerator.$(H): CustomSimpleAccessMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSimpleGetterMethodsCodeGenerator.$(O) CustomSimpleGetterMethodsCodeGenerator.$(H): CustomSimpleGetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomSimpleSetterMethodsCodeGenerator.$(O) CustomSimpleSetterMethodsCodeGenerator.$(H): CustomSimpleSetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomUITestCaseCodeGenerator.$(O) CustomUITestCaseCodeGenerator.$(H): CustomUITestCaseCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomTestCaseCodeGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomUITestCaseSetUpCodeGenerator.$(O) CustomUITestCaseSetUpCodeGenerator.$(H): CustomUITestCaseSetUpCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomTestCaseSetUpCodeGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomValueHolderAccessMethodsCodeGenerator.$(O) CustomValueHolderAccessMethodsCodeGenerator.$(H): CustomValueHolderAccessMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomValueHolderGetterMethodsCodeGenerator.$(O) CustomValueHolderGetterMethodsCodeGenerator.$(H): CustomValueHolderGetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator.$(O) CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator.$(H): CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator.$(O) CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator.$(H): CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator.$(O) CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator.$(H): CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomVisitorCodeGeneratorAcceptVisitor.$(O) CustomVisitorCodeGeneratorAcceptVisitor.$(H): CustomVisitorCodeGeneratorAcceptVisitor.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomVisitorCodeGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)CustomJavaScriptSimpleSetterMethodsCodeGenerator.$(O) CustomJavaScriptSimpleSetterMethodsCodeGenerator.$(H): CustomJavaScriptSimpleSetterMethodsCodeGenerator.st $(INCLUDE_TOP)\jn\refactoring_custom\CustomAccessMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGenerator.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomCodeGeneratorOrRefactoring.$(H) $(INCLUDE_TOP)\jn\refactoring_custom\CustomSimpleSetterMethodsCodeGenerator.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(STCHDR)
$(OUTDIR)extensions.$(O): extensions.st $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\changes\AddClassChange.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\changes\AddMethodChange.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\changes\RefactoryChange.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\changes\RefactoryClassChange.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\helpers\RBAbstractClass.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\helpers\RBClass.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\helpers\RBMetaclass.$(H) $(INCLUDE_TOP)\stx\goodies\refactoryBrowser\helpers\RBMethod.$(H) $(INCLUDE_TOP)\stx\libbasic\Object.$(H) $(INCLUDE_TOP)\stx\libtool\SystemBrowser.$(H) $(INCLUDE_TOP)\stx\libtool\Tools__NewSystemBrowser.$(H) $(INCLUDE_TOP)\stx\libview2\ApplicationModel.$(H) $(INCLUDE_TOP)\stx\libview2\Model.$(H) $(STCHDR)

# ENDMAKEDEPEND --- do not remove this line

# **Must be at end**

# Enforce recompilation of package definition class if Mercurial working
# copy state changes. Together with --guessVersion it ensures that package
# definition class always contains correct binary revision string.
!IFDEF HGROOT
$(OUTDIR)jn_refactoring_custom.$(O): $(HGROOT)\.hg\dirstate
!ENDIF
