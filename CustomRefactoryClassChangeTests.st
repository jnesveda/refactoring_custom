"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

TestCase subclass:#CustomRefactoryClassChangeTests
	instanceVariableNames:'change model'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Tests'
!

!CustomRefactoryClassChangeTests methodsFor:'initialization & release'!

setUp
    super setUp.

    model := RBNamespace new.
    change := RefactoryClassChange new
        model: model;
        yourself.

    "Modified: / 08-11-2014 / 14:29:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomRefactoryClassChangeTests methodsFor:'tests'!

test_change_class_with_existing_class
    | expectedClass actualClass |

    change changeClass: self class.

    expectedClass := self class.
    actualClass := change changeClass.

    self assert: expectedClass == actualClass

    "Created: / 08-11-2014 / 14:35:16 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_class_with_existing_class_with_rb_class
    | expectedClass actualClass rbClass |

    rbClass := model classFor: self class.  

    change changeClass: rbClass.

    expectedClass := self class.
    actualClass := change changeClass.

    self assert: expectedClass == actualClass

    "Created: / 08-11-2014 / 14:36:26 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_class_with_existing_metaclass
    | expectedClass actualClass |

    change changeClass: self class class.

    expectedClass := self class class.
    actualClass := change changeClass.

    self assert: expectedClass == actualClass

    "Created: / 08-11-2014 / 14:35:30 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_class_with_non_existing_class
    | expectedClass actualClass |

    change model: nil.

    change changeClass: (RBClass new
        name: #DummyClass01;
        yourself).

    self assert: (Smalltalk at: #DummyClass01) isNil.

    expectedClass := nil.
    actualClass := change changeClass.

    self assert: expectedClass == actualClass

    "Created: / 08-11-2014 / 14:38:20 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_class_with_non_existing_class_but_model_class
    | expectedClass actualClass class |

    model defineClass: 'Object subclass:#DummyClassForTestCase01
        instanceVariableNames:''''
        classVariableNames:''''
        poolDictionaries:''''
        category:'''''.

    self assert: (Smalltalk at: #DummyClassForTestCase01) isNil.

    class := model classNamed: #DummyClassForTestCase01.

    change changeClass: class.

    expectedClass := class.
    actualClass := change changeClass.

    self assert: expectedClass == actualClass

    "Created: / 08-11-2014 / 14:27:39 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_class_with_non_existing_metaclass_but_model_metaclass
    | expectedClass actualClass class |

    model defineClass: 'Object subclass:#DummyClassForTestCase01
        instanceVariableNames:''''
        classVariableNames:''''
        poolDictionaries:''''
        category:'''''.

    self assert: (Smalltalk at: #DummyClassForTestCase01) isNil.

    class := model metaclassNamed: #DummyClassForTestCase01.

    change changeClass: class.

    expectedClass := class.
    actualClass := change changeClass.

    self assert: expectedClass == actualClass

    "Created: / 08-11-2014 / 14:33:51 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

