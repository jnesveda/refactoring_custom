"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomSimpleGetterMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomSimpleGetterMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description

    ^ 'Getter methods for selected instance variables'

    "Modified: / 11-05-2014 / 16:37:33 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

group
    "Returns a collection strings describing a group to which
     receiver belongs. A groups may be nested hence the array of
     strings. For example for subgroup 'Accessors' in group 'Generators'
     this method should return #('Generators' 'Accessors')."

    "/ By default return an empty array which means the item will appear
    "/ in top-level group.
    ^ #('Accessors' 'Getters')

    "Created: / 22-08-2014 / 18:56:03 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label

    ^ 'Getter Method(s)'

    "Modified (format): / 11-05-2014 / 16:29:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSimpleGetterMethodsCodeGenerator methodsFor:'code generation'!

sourceForClass: aClass variableName: aName
    "Returns simple getter method source code for given class and variable name"

    | methodName comment |

    methodName := self methodNameFor: aName.
    comment := ''.

    userPreferences generateCommentsForGetters ifTrue:[
        | varType |

        varType := self varTypeOf: aName class: aClass.
        comment := '"return the %1 variable ''%2'' (automatically generated)"'.
        comment := comment bindWith: varType with: aName.
    ].  

    ^ self sourceCodeGenerator
        source: '`@methodName
        `"comment

        ^ `variableName';
        replace: '`@methodName' with: methodName asSymbol;
        replace: '`variableName' with: aName asString;
        replace: '`"comment' with: comment;
        newSource.

    "Created: / 19-05-2014 / 20:32:34 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 19-09-2014 / 22:35:39 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

