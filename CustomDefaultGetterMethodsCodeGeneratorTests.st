"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomDefaultGetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomDefaultGetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomDefaultGetterMethodsCodeGenerator new
! !

!CustomDefaultGetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_default_getter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: true.

    expectedSource := 'defaultInstanceVariable
    "default value for the ''instanceVariable'' instance variable (automatically generated)"

    self shouldImplement.
    ^ nil'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertClassMethodSource: expectedSource atSelector: #defaultInstanceVariable

    "Created: / 30-06-2014 / 13:31:59 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 11-07-2014 / 20:11:22 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_default_getter_method_generated_without_comments
    | expectedSource |

    userPreferences generateCommentsForGetters: false.

    expectedSource := 'defaultInstanceVariable
    self shouldImplement.
    ^ nil'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertClassMethodSource: expectedSource atSelector: #defaultInstanceVariable

    "Created: / 30-06-2014 / 11:31:06 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 11-07-2014 / 20:11:31 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

