"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

LibraryDefinition subclass:#jn_refactoring_custom
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'* Projects & Packages *'
!

!jn_refactoring_custom class methodsFor:'documentation'!

documentation
"
    Package documentation:

    API for code generation and refactoring

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>
"
! !

!jn_refactoring_custom class methodsFor:'description'!

excludedFromPreRequisites
    "list all packages which should be ignored in the automatic
     preRequisites scan. See #preRequisites for more."

    ^ #(
    )
!

mandatoryPreRequisites
    "list packages which are mandatory as a prerequisite.
     This are packages containing superclasses of my classes and classes which
     are extended by myself.
     They are mandatory, because we need these packages as a prerequisite for loading and compiling.
     This method is generated automatically,
     by searching along the inheritance chain of all of my classes."

    ^ #(
        #'stx:goodies/refactoryBrowser/changes'    "AddClassChange - extended"
        #'stx:goodies/refactoryBrowser/helpers'    "RBAbstractClass - extended"
        #'stx:goodies/refactoryBrowser/parser'    "ParseTreeRewriter - superclass of CustomParseTreeRewriter"
        #'stx:goodies/sunit'    "TestAsserter - superclass of CustomAccessMethodsCodeGeneratorTests"
        #'stx:libbasic'    "LibraryDefinition - superclass of jn_refactoring_custom"
        #'stx:libtool'    "CodeGenerator - superclass of CustomSourceCodeGenerator"
        #'stx:libview2'    "ApplicationModel - extended"
    )
!

referencedPreRequisites
    "list packages which are a prerequisite, because they contain
     classes which are referenced by my classes.
     We do not need these packages as a prerequisite for loading or compiling.
     This method is generated automatically,
     by searching all classes (and their packages) which are referenced by my classes."

    ^ #(
        #'jn:refactoring_custom/patches'    "CustomDummyClassPatches - referenced by CustomDummyTests>>test_dummy"
        #'stx:libbasic3'    "Change - referenced by CustomCodeGeneratorOrRefactoringTestCase>>assertSource:sameAs:"
        #'stx:libcomp'    "Parser - referenced by CustomNamespace>>createMethodImmediate:protocol:source:package:"
        #'stx:libjava'    "JavaClass - referenced by CustomCodeGeneratorOrRefactoringTests>>test_available_for_programming_languages_in_context_filled_with_class_perspective_java"
        #'stx:libjava/tools'    "JavaCompiler - referenced by CustomRBAbstractClassTests>>test_compiler_class_java"
        #'stx:libjavascript'    "JavaScriptCompiler - referenced by CustomJavaScriptSimpleSetterMethodsCodeGeneratorTests>>setUp"
        #'stx:libview'    "WindowGroup - referenced by CustomCodeGeneratorOrRefactoring>>executeInContextWithWaitCursor:"
        #'stx:libwidg'    "DialogBox - referenced by CustomCodeGeneratorOrRefactoringTests>>test_execute_in_context_aborted"
        #'stx:libwidg2'    "CheckBox - referenced by CustomDialog>>addCheckBoxOn:labeled:"
    )
!

subProjects
    "list packages which are known as subprojects.
     The generated makefile will enter those and make there as well.
     However: they are not forced to be loaded when a package is loaded;
     for those, redefine requiredPrerequisites"

    ^ #(
        #'jn:refactoring_custom/patches'
    )

    "Modified: / 15-10-2014 / 00:02:17 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

!jn_refactoring_custom class methodsFor:'description - contents'!

classNamesAndAttributes
    "lists the classes which are to be included in the project.
     Each entry in the list may be: a single class-name (symbol),
     or an array-literal consisting of class name and attributes.
     Attributes are: #autoload or #<os> where os is one of win32, unix,..."

    ^ #(
        "<className> or (<className> attributes...) in load order"
        (CustomBrowserEnvironmentTests autoload)
        CustomChangeManager
        CustomClassQuery
        (CustomClassQueryTests autoload)
        CustomCodeGeneratorOrRefactoring
        (CustomCodeGeneratorOrRefactoringTestCase autoload)
        CustomContext
        (CustomContextTests autoload)
        CustomDialog
        (CustomLocalChangeManagerTests autoload)
        CustomManager
        (CustomManagerTests autoload)
        CustomMenuBuilder
        (CustomMenuBuilderTests autoload)
        CustomMock
        (CustomMockTests autoload)
        CustomNamespace
        (CustomNewSystemBrowserTests autoload)
        (CustomNoneSourceCodeFormatterTests autoload)
        CustomParseTreeRewriter
        (CustomParseTreeRewriterTests autoload)
        CustomPerspective
        (CustomPerspectiveTests autoload)
        (CustomRBAbstractClassTests autoload)
        (CustomRBClassTests autoload)
        (CustomRBLocalSourceCodeFormatterTests autoload)
        (CustomRBMetaclassTests autoload)
        (CustomRBMethodTests autoload)
        CustomRefactoryBuilder
        (CustomRefactoryClassChangeTests autoload)
        CustomSourceCodeFormatter
        CustomSourceCodeGenerator
        (CustomSourceCodeGeneratorTests autoload)
        CustomSourceCodeSelection
        (CustomSourceCodeSelectionTests autoload)
        (CustomSubContextTests autoload)
        CustomTestCaseHelper
        (CustomTestCaseHelperTests autoload)
        #'jn_refactoring_custom'
        (CustomAccessMethodsCodeGeneratorTests autoload)
        (CustomAddClassChangeTests autoload)
        (CustomAddMethodChangeTests autoload)
        CustomBrowserChangeManager
        CustomBrowserContext
        (CustomBrowserContextTests autoload)
        (CustomChangeNotificationAccessMethodsCodeGeneratorTests autoload)
        (CustomChangeNotificationSetterMethodsCodeGeneratorTests autoload)
        CustomCodeGenerator
        (CustomCodeGeneratorClassGeneratorTests autoload)
        (CustomCodeGeneratorOrRefactoringTests autoload)
        (CustomCodeGeneratorTests autoload)
        (CustomCodeGeneratorUserPreferencesTests autoload)
        (CustomCodeSelectionToResourceTranslationTests autoload)
        (CustomDefaultGetterMethodsCodeGeneratorTests autoload)
        (CustomIsAbstractCodeGeneratorTests autoload)
        (CustomJavaScriptSimpleSetterMethodsCodeGeneratorTests autoload)
        (CustomLazyInitializationAccessMethodsCodeGeneratorTests autoload)
        (CustomLazyInitializationGetterMethodsCodeGeneratorTests autoload)
        CustomLocalChangeManager
        (CustomMultiSetterMethodsCodeGeneratorTests autoload)
        (CustomNamespaceTests autoload)
        (CustomNewClassGeneratorTests autoload)
        CustomNoneSourceCodeFormatter
        CustomRBLocalSourceCodeFormatter
        CustomRefactoring
        (CustomRefactoringClassGeneratorTests autoload)
        (CustomRefactoryBuilderTests autoload)
        (CustomReplaceIfNilWithIfTrueRefactoringTests autoload)
        CustomSilentDialog
        (CustomSimpleAccessMethodsCodeGeneratorTests autoload)
        (CustomSimpleGetterMethodsCodeGeneratorTests autoload)
        (CustomSimpleSetterMethodsCodeGeneratorTests autoload)
        CustomSubContext
        (CustomSubclassResponsibilityCodeGeneratorTests autoload)
        (CustomTestCaseCodeGeneratorTests autoload)
        (CustomTestCaseMethodCodeGeneratorTests autoload)
        (CustomTestCaseSetUpCodeGeneratorTests autoload)
        (CustomTestCaseTearDownCodeGeneratorTests autoload)
        (CustomUpdateTestCaseCategoryRefactoringTests autoload)
        CustomUserDialog
        (CustomValueHolderAccessMethodsCodeGeneratorTests autoload)
        (CustomValueHolderGetterMethodsCodeGeneratorTests autoload)
        (CustomValueHolderWithChangeNotificationAccessMethodsCodeGeneratorTests autoload)
        (CustomValueHolderWithChangeNotificationGetterMethodsCodeGeneratorTests autoload)
        (CustomValueHolderWithChangeNotificationSetterMethodsCodeGeneratorTests autoload)
        (CustomVisitorCodeGeneratorAcceptVisitorTests autoload)
        (CustomVisitorCodeGeneratorTests autoload)
        CustomAccessMethodsCodeGenerator
        CustomCodeSelectionRefactoring
        CustomIsAbstractCodeGenerator
        CustomJavaSimpleSetterMethodsCodeGenerator
        CustomNewClassGenerator
        CustomReplaceIfNilWithIfTrueRefactoring
        CustomSubclassResponsibilityCodeGenerator
        CustomTestCaseCodeGenerator
        CustomTestCaseMethodCodeGenerator
        CustomTestCaseSetUpCodeGenerator
        CustomTestCaseTearDownCodeGenerator
        CustomUpdateTestCaseCategoryRefactoring
        CustomVisitorCodeGenerator
        CustomChangeNotificationAccessMethodsCodeGenerator
        CustomChangeNotificationSetterMethodsCodeGenerator
        CustomCodeGeneratorClassGenerator
        CustomCodeGeneratorOrRefactoringTestCaseCodeGenerator
        CustomCodeSelectionToResourceTranslation
        CustomDefaultGetterMethodsCodeGenerator
        CustomLazyInitializationAccessMethodsCodeGenerator
        CustomLazyInitializationGetterMethodsCodeGenerator
        CustomMultiSetterMethodsCodeGenerator
        CustomPrintCodeSelectionRefactoring
        CustomRefactoringClassGenerator
        CustomSimpleAccessMethodsCodeGenerator
        CustomSimpleGetterMethodsCodeGenerator
        CustomSimpleSetterMethodsCodeGenerator
        CustomUITestCaseCodeGenerator
        CustomUITestCaseSetUpCodeGenerator
        CustomValueHolderAccessMethodsCodeGenerator
        CustomValueHolderGetterMethodsCodeGenerator
        CustomValueHolderWithChangeNotificationAccessMethodsCodeGenerator
        CustomValueHolderWithChangeNotificationGetterMethodsCodeGenerator
        CustomValueHolderWithChangeNotificationSetterMethodsCodeGenerator
        CustomVisitorCodeGeneratorAcceptVisitor
        CustomJavaScriptSimpleSetterMethodsCodeGenerator
    )
!

extensionMethodNames
    "lists the extension methods which are to be included in the project.
     Entries are pairwise elements, consisting of class-name and selector."

    ^ #(
        #'Tools::NewSystemBrowser' selectorMenuExtensionCustomRefactorings:
        #'Tools::NewSystemBrowser' classMenuExtensionCustomGenerators:
        #'Tools::NewSystemBrowser' codeViewMenuExtensionCustomRefactorings:
        #'Tools::NewSystemBrowser' selectorMenuExtensionCustomGenerators:
        #'Tools::NewSystemBrowser' variablesMenuExtensionCustomGenerators:
        RBAbstractClass allClassVarNames
        RBAbstractClass allSuperclassesDo:
        RBAbstractClass instVarNames
        RBAbstractClass methodDictionary
        RBAbstractClass nameWithoutPrefix
        RBAbstractClass superclassName:
        RBAbstractClass theMetaclass
        RBAbstractClass theNonMetaclass
        RBAbstractClass withAllSuperclassesDo:
        RBClass compile
        RBClass theNonMetaClass
        RBMetaclass theMetaClass
        RBMetaclass theMetaclass
        RBAbstractClass instVarNames:
        RBMethod sends:or:
        RBMethod category:
        RBMethod class:
        RBMethod compile
        RBMethod methodArgNames
        RBMethod methodDefinitionTemplate
        RBMethod model
        RBMethod model:
        RBMethod protocol
        RBMethod protocol:
        RBMethod replace:with:
        RBMethod sourceCodeGenerator
        RBMethod sourceCodeGenerator:
        AddClassChange package
        AddClassChange package:
        AddMethodChange package:
        RBAbstractClass compileMethod:
        RBAbstractClass package
        RBAbstractClass package:
        RBMethod newSource
        RBMethod package:
        RBAbstractClass inheritsFrom:
        RBAbstractClass isSubclassOf:
        RBAbstractClass instAndClassMethodsDo:
        RBAbstractClass methodsDo:
        RefactoryChange model
        RefactoryChange model:
        #'Tools::NewSystemBrowser' classMenuExtensionCustomRefactorings:
        AddClassChange argumensBySelectorPartsFromMessage:
        AddClassChange privateInClassName
        AddClassChange privateInClassName:
        RBAbstractClass compilerClass
        RBAbstractClass isLoaded
        RBAbstractClass privateClassesAt:
        RBAbstractClass realSharedPoolNames
        RBAbstractClass topNameSpace
        RBMetaclass owningClass
        RBMetaclass owningClass:
        RBMethod mclass
        RBAbstractClass owningClass
        RBAbstractClass owningClass:
        RBAbstractClass owningClassOrYourself
        RBAbstractClass topOwningClass
        RBMetaclass topOwningClass
        RBAbstractClass isAbstract:
        RBAbstractClass programmingLanguage
        RBMethod programmingLanguage
        #'Tools::NewSystemBrowser' classMenuExtensionNavigateToTestCase:
        #'Tools::NewSystemBrowser' customMenuBuilder
        RBAbstractClass sourceCodeAt:
    )
! !

!jn_refactoring_custom class methodsFor:'description - project information'!

companyName
    "Return a companyname which will appear in <lib>.rc"

    ^ 'My Company'
!

description
    "Return a description string which will appear in vc.def / bc.def"

    ^ 'Class Library'
!

legalCopyright
    "Return a copyright string which will appear in <lib>.rc"

    ^ 'My CopyRight or CopyLeft'
!

productName
    "Return a product name which will appear in <lib>.rc"

    ^ 'ProductName'
! !

!jn_refactoring_custom class methodsFor:'documentation'!

version_HG
    ^ '$Changeset: <not expanded> $'
! !

