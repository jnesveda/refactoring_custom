"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomTestCaseSetUpCodeGenerator subclass:#CustomUITestCaseSetUpCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomUITestCaseSetUpCodeGenerator class methodsFor:'accessing-presentation'!

description
    ^ 'Generates initial #setUp for UI test cases'

    "Created: / 16-09-2014 / 11:24:05 / Jan Vrany <jan.vrany@fit.cvut.cz>"
!

label
    ^ 'TestCase setUp method (for UI Test Cases)'

    "Created: / 16-09-2014 / 11:23:50 / Jan Vrany <jan.vrany@fit.cvut.cz>"
! !

