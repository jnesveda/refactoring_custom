"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomUpdateTestCaseCategoryRefactoringTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Refactorings-Tests'
!

!CustomUpdateTestCaseCategoryRefactoringTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomUpdateTestCaseCategoryRefactoring new
! !

!CustomUpdateTestCaseCategoryRefactoringTests methodsFor:'tests'!

test_available_for_class_fake_test

    |class|

    class := model createClassImmediate: #Dummy01Tests.  

    self deny: (generatorOrRefactoring class availableForClass: class).

    "Created: / 08-11-2014 / 23:40:58 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_for_class_object
    
    self deny: (generatorOrRefactoring class availableForClass: Object).

    "Created: / 08-11-2014 / 23:40:01 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_for_class_test_case
    
    self assert: (generatorOrRefactoring class availableForClass: self class).

    "Modified: / 08-11-2014 / 23:39:22 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_in_context_empty
    
    self deny: (generatorOrRefactoring class availableInContext: context).

    "Modified: / 08-11-2014 / 23:42:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_in_context_with_test_class

    context selectedClasses: (Array with: Object with: self class).  

    self assert: (generatorOrRefactoring class availableInContext: context).

    "Created: / 08-11-2014 / 23:43:29 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_in_context_without_test_class

    context selectedClasses: (Array with: Object).  

    self deny: (generatorOrRefactoring class availableInContext: context).

    "Created: / 08-11-2014 / 23:44:03 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_available_in_perspective
    
    self assert: (generatorOrRefactoring class availableInPerspective: CustomPerspective classPerspective).

    "Modified: / 08-11-2014 / 23:45:17 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_category_updated_model_classes
    | expectedCategory actualCategory testClass |

    model createClass
        name: #DummyClass01;
        category: #Category01;
        compile.

    testClass := model createClass
        name: #DummyClass01Tests;
        superclassName: #TestCase;
        category: #Category02;
        compile;
        yourself.  

    expectedCategory := #'Category01-Tests'.

    context selectedClasses: (Array with: testClass).  

    generatorOrRefactoring executeInContext: context.

    actualCategory := testClass category.

    self assert: expectedCategory = actualCategory

    "Created: / 08-11-2014 / 23:57:26 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 25-01-2015 / 16:16:21 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_category_updated_model_classes_category_ok
    | expectedCategory actualCategory testClass |

    model createClass
        name: #DummyClass01;
        category: #Category01;
        compile.

    testClass := model createClass
        name: #DummyClass01Tests;
        superclassName: #TestCase;
        category: #'Category01-Tests';
        compile;
        yourself.  

    expectedCategory := #'Category01-Tests'.

    context selectedClasses: (Array with: testClass).  

    generatorOrRefactoring executeInContext: context.

    actualCategory := testClass category.

    self assert: expectedCategory = actualCategory

    "Created: / 08-11-2014 / 23:58:40 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 25-01-2015 / 16:16:32 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_category_updated_model_classes_wrong_suffix
    | expectedCategory actualCategory testClass |

    model createClass
        name: #DummyClass01;
        category: #Category01;
        compile.

    testClass := model createClass
        name: #DummyClass01Tested;
        superclassName: #TestCase;
        category: #Category01;
        compile;
        yourself.  

    expectedCategory := #Category01.

    context selectedClasses: (Array with: testClass).  

    generatorOrRefactoring executeInContext: context.

    actualCategory := testClass category.

    self assert: expectedCategory = actualCategory

    "Created: / 09-11-2014 / 00:01:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 25-01-2015 / 16:16:42 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_category_updated_real_classes
    | expectedCategory actualCategory testClass |

    model createClassImmediate: #DummyClass01 category: #Category01.
    testClass := model createClassImmediate: #DummyClass01Tests superClassName: #TestCase.
    testClass category: #Category02.  

    expectedCategory := #'Category01-Tests'.

    context selectedClasses: (Array with: testClass).  

    generatorOrRefactoring executeInContext: context.

    actualCategory := testClass category.

    self assert: expectedCategory = actualCategory

    "Created: / 08-11-2014 / 23:53:04 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 25-01-2015 / 15:54:44 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

