"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomChangeNotificationAccessMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomChangeNotificationAccessMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomChangeNotificationAccessMethodsCodeGenerator new
! !

!CustomChangeNotificationAccessMethodsCodeGeneratorTests methodsFor:'tests'!

test_change_notification_access_methods_generated_with_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: true;
        generateCommentsForSetters: true.

    expectedGetterSource := 'instanceVariable
    "return the instance variable ''instanceVariable'' (automatically generated)"

    ^ instanceVariable'.

    expectedSetterSource := 'instanceVariable:something 
    "set the value of the instance variable ''instanceVariable'' and send a change notification (automatically generated)"

    (instanceVariable ~~ something) ifTrue:[
        instanceVariable := something.
        self changed:#instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 13-07-2014 / 16:45:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_change_notification_access_methods_generated_without_comments
    | expectedGetterSource expectedSetterSource |

    userPreferences
        generateCommentsForGetters: false;
        generateCommentsForSetters: false.

    expectedGetterSource := 'instanceVariable
    ^ instanceVariable'.

    expectedSetterSource := 'instanceVariable:something 
    (instanceVariable ~~ something) ifTrue:[
        instanceVariable := something.
        self changed:#instanceVariable.
    ].'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedGetterSource atSelector: #instanceVariable.
    self assertMethodSource: expectedSetterSource atSelector: #instanceVariable:

    "Created: / 13-07-2014 / 16:47:46 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomChangeNotificationAccessMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

