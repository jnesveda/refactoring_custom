"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

TestCase subclass:#CustomBrowserEnvironmentTests
	instanceVariableNames:'environment'
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Tests'
!

!CustomBrowserEnvironmentTests methodsFor:'initialization & release'!

setUp

    environment := BrowserEnvironment new.

    "Modified: / 05-11-2014 / 21:29:20 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomBrowserEnvironmentTests methodsFor:'tests'!

test_which_category_includes_existing_class
    | expectedCategory actualCategory className |

    expectedCategory := self class category.

    className := self class name.
    actualCategory := environment whichCategoryIncludes: className.

    self assert: expectedCategory = actualCategory.

    "Created: / 05-11-2014 / 21:33:34 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_which_category_includes_non_existing_class
    | expectedCategory actualCategory className |

    expectedCategory := nil.

    className := #DummyClassForTestCase01.
    self assert: (Smalltalk at: className) isNil.

    actualCategory := environment whichCategoryIncludes: className.

    self assert: expectedCategory = actualCategory.

    "Created: / 05-11-2014 / 21:32:27 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

