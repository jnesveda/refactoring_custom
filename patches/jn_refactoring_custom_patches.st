"{ Package: 'jn:refactoring_custom/patches' }"

"{ NameSpace: Smalltalk }"

LibraryDefinition subclass:#jn_refactoring_custom_patches
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'* Projects & Packages *'
!

!jn_refactoring_custom_patches class methodsFor:'documentation'!

documentation
"
    Package documentation:

    In this package are located custom extensions which overwrites
    already defined methods in stx (basically refactoring support).

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>
"
! !

!jn_refactoring_custom_patches class methodsFor:'description'!

mandatoryPreRequisites
    "list packages which are mandatory as a prerequisite.
     This are packages containing superclasses of my classes and classes which
     are extended by myself.
     They are mandatory, because we need these packages as a prerequisite for loading and compiling.
     This method is generated automatically,
     by searching along the inheritance chain of all of my classes."

    ^ #(
        #'stx:goodies/refactoryBrowser/changes'    "AddClassChange - extended"
        #'stx:goodies/refactoryBrowser/helpers'    "BrowserEnvironment - extended"
        #'stx:libbasic'    "LibraryDefinition - superclass of jn_refactoring_custom_patches"
    )
!

referencedPreRequisites
    "list packages which are a prerequisite, because they contain
     classes which are referenced by my classes.
     We do not need these packages as a prerequisite for loading or compiling.
     This method is generated automatically,
     by searching all classes (and their packages) which are referenced by my classes."

    ^ #(
        #'stx:goodies/refactoryBrowser/browser'    "RefactoryBrowserPlatformSupport - referenced by BrowserEnvironment>>whichCategoryIncludes:"
        #'stx:goodies/refactoryBrowser/parser'    "RBParser - referenced by AddClassChange>>fillOutDefinition"
        #'stx:libcomp'    "Parser - referenced by RBMethod>>info"
    )
!

subProjects
    "list packages which are known as subprojects. 
     The generated makefile will enter those and make there as well.
     However: they are not forced to be loaded when a package is loaded; 
     for those, redefine requiredPrerequisites."

    ^ #(
    )
! !

!jn_refactoring_custom_patches class methodsFor:'description - contents'!

classNamesAndAttributes
    "lists the classes which are to be included in the project.
     Each entry in the list may be: a single class-name (symbol),
     or an array-literal consisting of class name and attributes.
     Attributes are: #autoload or #<os> where os is one of win32, unix,..."

    ^ #(
        "<className> or (<className> attributes...) in load order"
        CustomDummyClassPatches
        #'jn_refactoring_custom_patches'
    )
!

extensionMethodNames
    "list class/selector pairs of extensions.
     A correponding method with real names must be present in my concrete subclasses"

    ^ #(
        RBAbstractClass compile:classified:
        RBAbstractClass compileTree:classified:
        RBAbstractClass compileTree:usingSource:classified:
        RBMethod category
        RBMethod info
        RBMethod method
        AddClassChange primitiveExecute
        RBMethod package
        AddMethodChange asUndoOperation
        BrowserEnvironment whichCategoryIncludes:
        RefactoryClassChange changeClass
        RBAbstractClass categories
        AddClassChange fillOutDefinition
        AddClassChange isValidMessageName:
        AddClassChange isValidSubclassCreationMessage:
        RBAbstractClass name:
        RBAbstractClass isAbstract
        RBMethod selector
    )
! !

!jn_refactoring_custom_patches methodsFor:'documentation'!

extensionsVersion_HG

    ^ '$Changeset: <not expanded> $'
! !

!jn_refactoring_custom_patches class methodsFor:'documentation'!

version_HG
    ^ '$Changeset: <not expanded> $'
! !

