"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomRefactoringClassGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!

!CustomRefactoringClassGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomRefactoringClassGenerator new
! !

!CustomRefactoringClassGeneratorTests methodsFor:'tests'!

test_refactoring_subclass_created
    | expectedSource class |

    expectedSource := 'buildInContext:aCustomContext
    self shouldImplement'.

    generatorOrRefactoring
        newClassName: #DummyClass01;  
        executeInContext: context.  

    self assertClassExists: #DummyClass01.  

    class := Smalltalk at: #DummyClass01.

    self assertMethodSource:expectedSource atSelector:#buildInContext: forClass: class.  
    self assert: (class superclass name) = #CustomRefactoring.

    "Created: / 09-11-2014 / 01:04:37 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

