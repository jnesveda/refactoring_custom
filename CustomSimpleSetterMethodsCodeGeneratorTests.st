"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomCodeGeneratorOrRefactoringTestCase subclass:#CustomSimpleSetterMethodsCodeGeneratorTests
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators-Tests'
!


!CustomSimpleSetterMethodsCodeGeneratorTests methodsFor:'accessing'!

generatorOrRefactoring
    ^ CustomSimpleSetterMethodsCodeGenerator new
! !

!CustomSimpleSetterMethodsCodeGeneratorTests methodsFor:'tests'!

test_simple_setter_method_generated_with_comments
    | expectedSource |

    userPreferences generateCommentsForSetters: true.

    expectedSource := 'instanceVariable:something
    "set the value of the instance variable ''instanceVariable'' (automatically generated)"

    instanceVariable := something'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable:

    "Created: / 05-07-2014 / 13:29:34 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 05-07-2014 / 19:38:09 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

test_simple_setter_method_generated_without_comments
    "test setter method"

    | expectedSource |

    userPreferences generateCommentsForSetters: false.

    expectedSource := 'instanceVariable:something
    instanceVariable := something'.

    self executeGeneratorInContext: #classWithInstanceVariable.
    self assertMethodSource: expectedSource atSelector: #instanceVariable:

    "Created: / 05-07-2014 / 13:38:41 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
    "Modified: / 05-07-2014 / 19:38:17 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomSimpleSetterMethodsCodeGeneratorTests class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

