/*
 * $Header$
 *
 * DO NOT EDIT
 * automagically generated from the projectDefinition: jn_refactoring_custom_patches.
 */
#define __INDIRECTVMINITCALLS__
#include <stc.h>

#ifdef WIN32
# pragma codeseg INITCODE "INITCODE"
#endif

#if defined(INIT_TEXT_SECTION) || defined(DLL_EXPORT)
DLL_EXPORT void _libjn_refactoring_custom_patches_Init() INIT_TEXT_SECTION;
DLL_EXPORT void _libjn_refactoring_custom_patches_InitDefinition() INIT_TEXT_SECTION;
#endif

void _libjn_refactoring_custom_patches_InitDefinition(pass, __pRT__, snd)
OBJ snd; struct __vmData__ *__pRT__; {
__BEGIN_PACKAGE2__("libjn_refactoring_custom_patches__DFN", _libjn_refactoring_custom_patches_InitDefinition, "jn:refactoring_custom/patches");
_jn_137refactoring_137custom_137patches_Init(pass,__pRT__,snd);

__END_PACKAGE__();
}

void _libjn_refactoring_custom_patches_Init(pass, __pRT__, snd)
OBJ snd; struct __vmData__ *__pRT__; {
__BEGIN_PACKAGE2__("libjn_refactoring_custom_patches", _libjn_refactoring_custom_patches_Init, "jn:refactoring_custom/patches");
_CustomDummyClassPatches_Init(pass,__pRT__,snd);
_jn_137refactoring_137custom_137patches_Init(pass,__pRT__,snd);

_jn_137refactoring_137custom_137patches_extensions_Init(pass,__pRT__,snd);
__END_PACKAGE__();
}
