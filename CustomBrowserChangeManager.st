"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomChangeManager subclass:#CustomBrowserChangeManager
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom'
!

!CustomBrowserChangeManager class methodsFor:'documentation'!

documentation
"
    Implementation of global Browser refactory changes 
    - so you could see them for example in Browser - Operations menu.

    [author:]
        Jakub Nesveda <nesvejak@fit.cvut.cz>  

"
! !

!CustomBrowserChangeManager methodsFor:'performing-changes'!

performChange: aRefactoringChange

    RefactoryChangeManager instance performChange: aRefactoringChange

    "Created: / 31-05-2014 / 13:20:19 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

