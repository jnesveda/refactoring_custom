"{ Package: 'jn:refactoring_custom' }"

"{ NameSpace: Smalltalk }"

CustomAccessMethodsCodeGenerator subclass:#CustomChangeNotificationAccessMethodsCodeGenerator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Interface-Refactoring-Custom-Generators'
!

!CustomChangeNotificationAccessMethodsCodeGenerator class methodsFor:'accessing-presentation'!

description
    "Returns more detailed description of the receiver"
    
    ^ 'Access Method(s) with Change Notification for selected instance variables'

    "Modified: / 13-07-2014 / 16:39:56 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
!

label
    "Returns show label describing the receiver. This label
     is used in UI as menu item/tree item label."
    
    ^ 'Access Method(s) with Change Notification'

    "Modified: / 13-07-2014 / 16:39:51 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

!CustomChangeNotificationAccessMethodsCodeGenerator methodsFor:'executing'!

buildInContext: aCustomContext
    "Creates access methods with change notification in setter for given context"

    self executeSubGeneratorOrRefactoringClasses:(Array 
                  with:CustomSimpleGetterMethodsCodeGenerator
                  with:CustomChangeNotificationSetterMethodsCodeGenerator)
          inContext:aCustomContext

    "Modified: / 13-07-2014 / 16:39:08 / Jakub Nesveda <nesvejak@fit.cvut.cz>"
! !

